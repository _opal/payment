<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfers', function (Blueprint $table) {
            $table->string('uuid', 32);
            $table->integer('user_id')->unsigned();
            $table->decimal('amount', 10, 2);
            $table->string('currency', 3);
            $table->decimal('credited_amount', 10, 2);
            $table->string('credited_currency', 3);
            $table->string('res_status_code', 3);
            $table->string('res_status_description');
            $table->enum('status', ['initiated', 'pending', 'success', 'canceled', 'error', 'verified', 'completed']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transfers');
    }
}
