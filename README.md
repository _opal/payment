#Payment

Laravel package to support user payment with credit card

>Install

* composer install syotams/payment
* artisan vendor:publish
* artisan migrate
* add Syotams\Payment\PaymentServiceProvider::class to your providers list in app.php


edit payment.php

>Usage

$item = new Item('Deposit', 'Test Item');

$amount = new Amount(50, 'USD');

$customer = new Customer(1, 'Username', 'Lastname', 'user@test.com', 'ISR', 'ISR');

$payment = new Payment($item, $amount, $customer);

$token = PaymentFacade::createToken($payment);

PaymentFacade::redirectToPaymentPage($token);