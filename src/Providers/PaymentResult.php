<?php

namespace Syotams\Payment\Providers;

use Syotams\Payment\Contracts\Model\IPaymentResult;

/**
 * Created by PhpStorm.
 * User: yotam
 * Date: 1/30/17
 * Time: 3:36 PM
 */
class PaymentResult implements IPaymentResult
{
    private $amount;

    private $currency;

    private $status;


    public function __construct($amount, $currency, $status)
    {
        $this->amount = $amount;
        $this->currency = $currency;
        $this->status = $status;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function getCurrency()
    {
        return $this->currency;
    }

    public function getStatus()
    {
        return $this->status;
    }


}