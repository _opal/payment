<?php

namespace Syotams\Payment\Providers\MockProvider;
use Illuminate\Http\Request;
use Syotams\Payment\Application\Transfers\DefaultNotificationCommand;
use Syotams\Payment\Contracts\IInitialResponse;
use Syotams\Payment\Contracts\IPaymentProvider;
use Syotams\Payment\Contracts\Model\IPayment;
use Syotams\Payment\Contracts\Model\IPaymentResult;
use Syotams\Payment\Domain\Models\Transfers\Transfer;
use Syotams\Payment\Domain\Models\Transfers\TransferStatus;
use Syotams\Payment\External\Http\TransactionResponse;
use Syotams\Payment\Providers\PaymentResult;

class MockProvider implements IPaymentProvider
{
    public function initiateRequest(IPayment $payment, Transfer $transfer): IInitialResponse
    {
        return new class(1, 'Token created', 0) implements IInitialResponse
        {
            private $token;
            private $message;
            private $code;

            /**
             *  constructor.
             * @param $token
             * @param $message
             * @param $code
             */
            public function __construct($token, $message, $code)
            {
                $this->token = $token;
                $this->message = $message;
                $this->code = $code;
            }

            public function getErrorCode()
            {
                return 0;
            }

            public function getErrorMessage()
            {
                return 'Error';
            }

            public function getToken()
            {
                return $this->token;
            }

            public function getProviderOrderId()
            {
                return 0;
            }

            public function getResponse()
            {
                // TODO: Implement getResponse() method.
            }
        };
    }

    public function createTransactionResponse(Request $response): TransactionResponse
    {
        return new TransactionResponse(
            $response->get('uuid'),
            $response->get('message'),
            $response->get('code')
        );
    }

    public function handleSuccess(Request $response): TransactionResponse
    {
        return $this->createTransactionResponse($response);
    }

    public function handleCancel(Request $response): TransactionResponse
    {
        return $this->createTransactionResponse($response);
    }

    public function getPaymentFormUrl(IInitialResponse $response)
    {
        return '/transfers/process_transaction?token=' . $response->getToken();
    }

    public function verifyPayment(Transfer $transfer): IPaymentResult
    {
        return new PaymentResult(0, 0, TransferStatus::PENDING);
    }

    public function handleError(Request $response): TransactionResponse
    {
        return $this->createTransactionResponse($response);
    }

    public function mapToTransferStatus($code)
    {
        switch($code) {
            case 'INITIATED':
                return 'INITIATED';

            default:
            case 'FAILED':
                return 'FAILED';

            case 'TERMINATED':
                return 'TERMINATED';
        }
    }

    public function verifyTransaction(Transfer $transfer, DefaultNotificationCommand $command)
    {
        // TODO: Implement verifyTransaction() method.
    }

    public function verifyAmounts(Transfer $transfer, $amount, $currency)
    {
        return true;
    }
}