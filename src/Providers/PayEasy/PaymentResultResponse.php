<?php
/**
 * Created by PhpStorm.
 * User: yotam
 * Date: 4/25/18
 * Time: 3:13 PM
 */

namespace Syotams\Payment\Providers\PayEasy;


use Syotams\Payment\External\Http\TransactionResponse;

class PaymentResultResponse extends TransactionResponse
{

    private $response_signature;
    private $transaction_type;
//    private $status_severity_1;
//    private $status_code_1;
    private $completion_time_stamp;
    private $token_id;
    private $transaction_state;
    private $transaction_id;
    private $merchant_account_id;
    private $ip_address;
    private $request_id;
    private $requested_amount_currency;
    private $requested_amount;
    private $masked_account_number;
    private $status_description_1;
    private $authorization_code;

    /**
     * @return mixed
     */
    public function getResponseSignature()
    {
        return $this->response_signature;
    }

    /**
     * @param mixed $response_signature
     */
    public function setResponseSignature($response_signature)
    {
        $this->response_signature = $response_signature;
    }

    /**
     * @return mixed
     */
    public function getTransactionType()
    {
        return $this->transaction_type;
    }

    /**
     * @param mixed $transaction_type
     */
    public function setTransactionType($transaction_type)
    {
        $this->transaction_type = $transaction_type;
    }

    /**
     * @return mixed
     */
    public function getCompletionTimeStamp()
    {
        return $this->completion_time_stamp;
    }

    /**
     * @param mixed $completion_time_stamp
     */
    public function setCompletionTimeStamp($completion_time_stamp)
    {
        $this->completion_time_stamp = $completion_time_stamp;
    }

    /**
     * @return mixed
     */
    public function getTokenId()
    {
        return $this->token_id;
    }

    /**
     * @param mixed $token_id
     */
    public function setTokenId($token_id)
    {
        $this->token_id = $token_id;
    }

    /**
     * @return mixed
     */
    public function getTransactionState()
    {
        return $this->transaction_state;
    }

    /**
     * @param mixed $transaction_state
     */
    public function setTransactionState($transaction_state)
    {
        $this->transaction_state = $transaction_state;
    }

    /**
     * @return mixed
     */
    public function getTransactionId()
    {
        return $this->transaction_id;
    }

    /**
     * @param mixed $transaction_id
     */
    public function setTransactionId($transaction_id)
    {
        $this->transaction_id = $transaction_id;
    }

    /**
     * @return mixed
     */
    public function getMerchantAccountId()
    {
        return $this->merchant_account_id;
    }

    /**
     * @param mixed $merchant_account_id
     */
    public function setMerchantAccountId($merchant_account_id)
    {
        $this->merchant_account_id = $merchant_account_id;
    }

    /**
     * @return mixed
     */
    public function getIpAddress()
    {
        return $this->ip_address;
    }

    /**
     * @param mixed $ip_address
     */
    public function setIpAddress($ip_address)
    {
        $this->ip_address = $ip_address;
    }

    /**
     * @return mixed
     */
    public function getRequestId()
    {
        return $this->request_id;
    }

    /**
     * @param mixed $request_id
     */
    public function setRequestId($request_id)
    {
        $this->request_id = $request_id;
    }

    /**
     * @return mixed
     */
    public function getRequestedAmountCurrency()
    {
        return $this->requested_amount_currency;
    }

    /**
     * @param mixed $requested_amount_currency
     */
    public function setRequestedAmountCurrency($requested_amount_currency)
    {
        $this->requested_amount_currency = $requested_amount_currency;
    }

    /**
     * @return mixed
     */
    public function getRequestedAmount()
    {
        return $this->requested_amount;
    }

    /**
     * @param mixed $requested_amount
     */
    public function setRequestedAmount($requested_amount)
    {
        $this->requested_amount = $requested_amount;
    }

    /**
     * @return mixed
     */
    public function getMaskedAccountNumber()
    {
        return $this->masked_account_number;
    }

    /**
     * @param mixed $masked_account_number
     */
    public function setMaskedAccountNumber($masked_account_number)
    {
        $this->masked_account_number = $masked_account_number;
    }

    /**
     * @return mixed
     */
    public function getStatusDescription1()
    {
        return $this->status_description_1;
    }

    /**
     * @param mixed $status_description_1
     */
    public function setStatusDescription1($status_description_1)
    {
        $this->status_description_1 = $status_description_1;
    }

    /**
     * @return mixed
     */
    public function getAuthorizationCode()
    {
        return $this->authorization_code;
    }

    /**
     * @param mixed $authorization_code
     */
    public function setAuthorizationCode($authorization_code)
    {
        $this->authorization_code = $authorization_code;
    }

}