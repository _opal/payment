<?php
/**
 * Created by PhpStorm.
 * User: yotam
 * Date: 4/29/18
 * Time: 5:32 PM
 */

namespace Syotams\Payment\Providers\PayEasy\Responses;


class OrderStatusResponse
{

    private $ip;

    private $transaction_id;

    private $transaction_state;

    private $request_id;

    private $amount;

    private $currency;

    private $status_code;

    private $status_severity;

    private $status_description;


    public function __construct($data = [])
    {
        $this->transaction_id = array_get($data, 'transaction-id');
        $this->transaction_state = array_get($data, 'transaction-state');
        $this->request_id = array_get($data, 'request-id');
        $this->amount = array_get($data, 'requested-amount.value');
        $this->currency = array_get($data, 'requested-amount.currency');
        $this->ip = array_get($data, 'ip-address');

        $statuses = array_get($data, 'statuses.status');

        if(count($statuses)) {
            $this->status_code = array_get($statuses[0], 'code');
            $this->status_description = array_get($statuses[0], 'description');
            $this->status_severity = array_get($statuses[0], 'status_severity');
        }
    }

    /**
     * @return mixed
     */
    public function getTransactionId()
    {
        return $this->transaction_id;
    }

    /**
     * @return mixed
     */
    public function getTransactionState()
    {
        return $this->transaction_state;
    }

    /**
     * @return mixed
     */
    public function getRequestId()
    {
        return $this->request_id;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->status_code;
    }

    /**
     * @return mixed
     */
    public function getStatusSeverity()
    {
        return $this->status_severity;
    }

    /**
     * @return mixed
     */
    public function getStatusDescription()
    {
        return $this->status_description;
    }

    /**
     * @return mixed
     */
    public function getIp()
    {
        return $this->ip;
    }
}