<?php
/**
 * Created by PhpStorm.
 * User: yotam
 * Date: 4/29/18
 * Time: 5:25 PM
 */

namespace Syotams\Payment\Providers\PayEasy\Requests;


use Illuminate\Http\Request;
use Syotams\Payment\Contracts\IRequest;

class OrderStatusRequest implements IRequest
{

    private $url;

    private $endPoint;

    private $orderId;

    private $merchantAccountId;

    private $headers;


    public function __construct($url)
    {
        $this->setUrl($url);
        $this->headers = ['Accept' => 'application/json'];
    }

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        // TODO: Implement toArray() method.
    }

    function getURL()
    {
        return "{$this->url}/{$this->getEndPoint()}";
    }

    function getRequestMethod()
    {
        return Request::METHOD_GET;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @param mixed $endPoint
     */
    public function setEndPoint($endPoint)
    {
        $this->endPoint = $endPoint;
    }

    /**
     * @param mixed $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @param mixed $merchantAccountId
     */
    public function setMerchantAccountId($merchantAccountId)
    {
        $this->merchantAccountId = $merchantAccountId;
    }

    private function getEndPoint()
    {
        return "{$this->merchantAccountId}/payments/search?payment.request-id={$this->orderId}";
    }

    /**
     * @return mixed
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @param mixed $headers
     */
    public function setHeaders(array $headers)
    {
        $this->headers = $headers;
    }

}