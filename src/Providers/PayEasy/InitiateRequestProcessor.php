<?php
/**
 * Created by PhpStorm.
 * User: yotam
 * Date: 4/25/18
 * Time: 3:47 PM
 */

namespace Syotams\Payment\Providers\PayEasy;


use Carbon\Carbon;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Http\Request;
use JsonSerializable;
use Syotams\Payment\Contracts\IInitialResponse;

class InitiateRequestProcessor implements IInitialResponse, Jsonable, JsonSerializable
{

    private $url;

    private $request_time_stamp;

    private $request_id;

    private $merchant_account_id;

    private $requested_amount;

    private $requested_amount_currency;

    private $transaction_type;

    private $ip_address;

    private $payment_method = 'alogateway';

    private $secret_key;

    private $redirect_url;

    private $request_signature;

	private $order_detail;
	private $success_redirect_url;
	private $fail_redirect_url;
	private $cancel_redirect_url;
	private $processing_redirect_url;
	private $first_name;
	private $last_name;
	private $email;
	private $phone;
	private $street1;
	private $city;
	private $state;
	private $postal_code;
	private $country;


    public function createSignatureCode()
    {
        $this->request_signature =
              $this->request_time_stamp
            . $this->request_id
            . $this->merchant_account_id
            . $this->transaction_type
            . $this->requested_amount
            . $this->requested_amount_currency
            . $this->redirect_url
            . $this->ip_address
            . $this->secret_key;

        $this->request_signature = hash('sha256', trim($this->request_signature));

        return $this->request_signature;
    }

    /**
     * @param mixed $request_time_stamp
     */
    public function setRequestTimeStamp(Carbon $request_time_stamp)
    {
        $this->request_time_stamp = $request_time_stamp->format('YmdHis');
    }

    /**
     * @param mixed $request_id
     */
    public function setRequestId($request_id)
    {
        $this->request_id = $request_id;
    }

    /**
     * @param mixed $merchant_account_id
     */
    public function setMerchantAccountId($merchant_account_id)
    {
        $this->merchant_account_id = $merchant_account_id;
    }

    /**
     * @param mixed $requested_amount
     */
    public function setRequestedAmount($requested_amount)
    {
        $this->requested_amount = $requested_amount;
    }

    /**
     * @param mixed $requested_amount_currency
     */
    public function setRequestedAmountCurrency($requested_amount_currency)
    {
        $this->requested_amount_currency = $requested_amount_currency;
    }

    /**
     * @param mixed $transaction_type
     */
    public function setTransactionType($transaction_type)
    {
        $this->transaction_type = $transaction_type;
    }

    /**
     * @param mixed $ip_address
     */
    public function setIpAddress($ip_address)
    {
        $this->ip_address = $ip_address;
    }

    /**
     * @param mixed $payment_method
     */
    public function setPaymentMethod($payment_method)
    {
        $this->payment_method = $payment_method;
    }

    /**
     * @param mixed $secret_key
     */
    public function setSecretKey($secret_key)
    {
        $this->secret_key = $secret_key;
    }

    /**
     * @param mixed $request_signature
     */
    public function setRequestSignature($request_signature)
    {
        $this->request_signature = $request_signature;
    }

    /**
     * @param mixed $redirect_url
     */
    public function setRedirectUrl($redirect_url)
    {
        $this->redirect_url = $redirect_url;
    }

    /**
     * @param mixed $order_detail
     */
    public function setOrderDetail($order_detail)
    {
        $this->order_detail = $order_detail;
    }

    /**
     * @param mixed $success_redirect_url
     */
    public function setSuccessRedirectUrl($success_redirect_url)
    {
        $this->success_redirect_url = $success_redirect_url;
    }

    /**
     * @param mixed $fail_redirect_url
     */
    public function setFailRedirectUrl($fail_redirect_url)
    {
        $this->fail_redirect_url = $fail_redirect_url;
    }

    /**
     * @param mixed $cancel_redirect_url
     */
    public function setCancelRedirectUrl($cancel_redirect_url)
    {
        $this->cancel_redirect_url = $cancel_redirect_url;
    }

    /**
     * @param mixed $processing_redirect_url
     */
    public function setProcessingRedirectUrl($processing_redirect_url)
    {
        $this->processing_redirect_url = $processing_redirect_url;
    }

    /**
     * @param mixed $first_name
     */
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;
    }

    /**
     * @param mixed $last_name
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @param mixed $street1
     */
    public function setStreet1($street1)
    {
        $this->street1 = $street1;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @param mixed $postal_code
     */
    public function setPostalCode($postal_code)
    {
        $this->postal_code = $postal_code;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    public function getURL()
    {
        return $this->url;
    }

    public function getRequestMethod()
    {
        return Request::METHOD_POST;
    }

    /**
     * @param $formatted
     * @return mixed
     */
    public function getRequestTimeStamp()
    {
        return $this->request_time_stamp;
    }

    /**
     * @return mixed
     */
    public function getRequestId()
    {
        return $this->request_id;
    }

    /**
     * @return mixed
     */
    public function getMerchantAccountId()
    {
        return $this->merchant_account_id;
    }

    /**
     * @return mixed
     */
    public function getRequestedAmount()
    {
        return $this->requested_amount;
    }

    /**
     * @return mixed
     */
    public function getRequestedAmountCurrency()
    {
        return $this->requested_amount_currency;
    }

    /**
     * @return mixed
     */
    public function getTransactionType()
    {
        return $this->transaction_type;
    }

    /**
     * @return mixed
     */
    public function getIpAddress()
    {
        return $this->ip_address;
    }

    /**
     * @return string
     */
    public function getPaymentMethod(): string
    {
        return $this->payment_method;
    }

    /**
     * @return mixed
     */
    public function getSecretKey()
    {
        return $this->secret_key;
    }

    /**
     * @return mixed
     */
    public function getRedirectUrl()
    {
        return $this->redirect_url;
    }

    /**
     * @return mixed
     */
    public function getRequestSignature()
    {
        return $this->request_signature;
    }

    /**
     * @return mixed
     */
    public function getOrderDetail()
    {
        return $this->order_detail;
    }

    /**
     * @return mixed
     */
    public function getSuccessRedirectUrl()
    {
        return $this->success_redirect_url;
    }

    /**
     * @return mixed
     */
    public function getFailRedirectUrl()
    {
        return $this->fail_redirect_url;
    }

    /**
     * @return mixed
     */
    public function getCancelRedirectUrl()
    {
        return $this->cancel_redirect_url;
    }

    /**
     * @return mixed
     */
    public function getProcessingRedirectUrl()
    {
        return $this->processing_redirect_url;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return mixed
     */
    public function getStreet1()
    {
        return $this->street1;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @return mixed
     */
    public function getPostalCode()
    {
        return $this->postal_code;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    public function toArray()
    {
        $members = get_object_vars($this);
        unset($members['secret_key']);
        return $members;
    }

    /**
     * Convert the object to its JSON representation.
     *
     * @param  int $options
     * @return string
     */
    public function toJson($options = 0)
    {
        return json_encode($this->jsonSerialize(), $options);
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return $this->toArray();
    }

    public function getErrorCode()
    {
        // TODO: Implement getErrorCode() method.
    }

    public function getErrorMessage()
    {
        // TODO: Implement getErrorMessage() method.
    }

    public function getToken()
    {
        return $this->createSignatureCode();
    }

    public function getProviderOrderId()
    {
        // TODO: Implement getProviderOrderId() method.
    }

    public function getResponse()
    {
        // TODO: Implement getResponse() method.
    }

    public function setAPIUrl($processor_url)
    {
        $this->url = $processor_url;
    }
}