<?php
/**
 * Created by PhpStorm.
 * User: yotam
 * Date: 4/25/18
 * Time: 3:06 PM
 */

namespace Syotams\Payment\Providers\PayEasy;


use Illuminate\Http\Request;
use Syotams\Payment\Application\Transfers\DefaultNotificationCommand;
use Syotams\Payment\Contracts\IHttpRequestGateway;
use Syotams\Payment\Contracts\IInitialResponse;
use Syotams\Payment\Contracts\IPaymentProvider;
use Syotams\Payment\Contracts\Model\IPayment;
use Syotams\Payment\Contracts\Model\IPaymentResult;
use Syotams\Payment\Domain\Models\Transfers\Transfer;
use Syotams\Payment\Domain\Models\Transfers\TransferStatus;
use Syotams\Payment\External\Http\TransactionResponse;
use Syotams\Payment\Providers\PayEasy\Requests\OrderStatusRequest;
use Syotams\Payment\Providers\PayEasy\Responses\OrderStatusResponse;
use Syotams\Payment\Providers\PaymentResult;

class PayEasy implements IPaymentProvider
{

    private $httpRequestGateway;

    private $config;


    public function __construct(IHttpRequestGateway $httpRequestGateway, $config)
    {
        $this->httpRequestGateway = $httpRequestGateway;

        $this->config = $config;
    }

    public function initiateRequest(IPayment $payment, Transfer $transfer): IInitialResponse
    {
        $processor = new InitiateRequestProcessor();

        (new Mapper())->mapPaymentToRequest($processor, $payment);

        $processor->setRequestId($transfer->getUUID());
        $processor->setMerchantAccountId($this->config['merchant_account_id']);
        $processor->setAPIUrl($this->config['processor_url']);
        $processor->setRedirectUrl($this->config['processing_redirect_url']);
        $processor->setCancelRedirectUrl($this->config['cancel_redirect_url']);
        $processor->setSuccessRedirectUrl($this->config['success_redirect_url']);
        $processor->setFailRedirectUrl($this->config['fail_redirect_url']);
        $processor->setProcessingRedirectUrl($this->config['processing_redirect_url']);
        $processor->setSecretKey($this->config['secret_key']);

        return $processor;
    }

    public function getPaymentFormUrl(IInitialResponse $response) {
        if($response instanceof InitiateRequestProcessor) {
            return $response->getURL();
        }

        return null;
    }

    public function createTransactionResponse(Request $response): TransactionResponse
    {
        $paymentResponse = new PaymentResultResponse(
            $response->get('request_id'),
            $response->get('status_severity_1'),
            $response->get('status_code')
        );

        $paymentResponse->setTransactionState($response->get('transaction_state'));
        $paymentResponse->setResponseSignature($response->get('response_signature'));
        $paymentResponse->setStatusDescription1($response->get('status_description_1'));

        return $paymentResponse;
    }

    /**
     * @param Transfer $transfer
     * @return IPaymentResult
     * @throws \Exception
     */
    public function verifyPayment(Transfer $transfer): IPaymentResult
    {
        $request = new OrderStatusRequest($this->config['api_url']);

        $request->setMerchantAccountId($this->config['merchant_account_id']);
        $request->setOrderId($transfer->getUUID());

        $authorizationBasic = base64_encode($this->config['basic_authorization']);

        $request->setHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Basic ' . $authorizationBasic
        ]);

        $body = $this->httpRequestGateway->request($request);
        $body = preg_replace("/[\r\n]*/","",$body);

        $paramsArr = json_decode($body, true);

        $response = new OrderStatusResponse(array_get($paramsArr, 'payment'));

        if(in_array($response->getTransactionState(), ['failed'])) {
            throw new \Exception(
                $response->getStatusSeverity() . ': ' . $response->getStatusDescription(),
                $response->getStatusCode());
        }

        return new PaymentResult(
            $response->getAmount(),
            $response->getCurrency(),
            $this->mapToTransferStatus($response->getStatusCode())
        );
    }

    public function mapToTransferStatus($code)
    {
        $code = strtoupper($code);

        switch ($code) {
            case '500.3054':
                return TransferStatus::PENDING;

            case '200.0000': // Request successful
            case '200.1078': // Successful with 3D Full Authentication
            case '201.0000': // Creation Success
                return TransferStatus::VERIFIED;

            case '500.1076': // Customer failed or Cancelled authentication.
            case '500.1108': // Transaction was cancelled/aborted.
            case '500.2494': // cancelled on request
            case '500.2495': // cancellation order executed
            case '500.3026': // Failed - Cancelled by consumer
                return TransferStatus::CANCELED;

            case '500.1109':
            case '500.3048': // Request not successful. Transaction declined.
                return TransferStatus::DECLINED;

            case '500.4025': // Parent transaction amount mismatch. Autoreversal successfull.
            case '400.1013': // Requested Amount Below Minimum

                return TransferStatus::ERROR;

            default:
                throw new \InvalidArgumentException();
        }
    }

    public function verifyTransaction(Transfer $transfer, DefaultNotificationCommand $command)
    {
        return false; // not implementing notifications
    }

    public function verifyAmounts(Transfer $transfer, $amount, $currency)
    {
        // It was asked by Ran not to no verify amounts
        return true; // $transfer->getCurrency() == $currency and $transfer->getAmount() == $amount;
    }
}