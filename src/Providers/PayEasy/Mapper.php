<?php
/**
 * Created by PhpStorm.
 * User: yotam
 * Date: 4/29/18
 * Time: 3:48 PM
 */

namespace Syotams\Payment\Providers\PayEasy;


use Carbon\Carbon;
use Illuminate\Support\Facades\Request;
use Syotams\Payment\Contracts\Model\IPayment;

class Mapper
{

    public function mapPaymentToRequest(InitiateRequestProcessor $processor, IPayment $payment)
    {
        $amount     = $payment->getAmount();
        $customer   = $payment->getCustomer();
        $item       = $payment->getItem();

        // core

        $processor->setRequestTimeStamp(Carbon::now());
        $processor->setIpAddress(Request::ip());
        $processor->setTransactionType('debit');


        // customer params

        $processor->setFirstName($customer->getFirstName());
        $processor->setLastName($customer->getLastName());
        $processor->setStreet1($customer->getAddress());
        $processor->setCity($customer->getCity());
        $processor->setPostalCode($customer->getZipCode());
        $processor->setCountry($customer->getCountry());
        $processor->setPhone($customer->getPhone());
        $processor->setEmail($customer->getEmail());


        // item params

        $processor->setOrderDetail($item->getName());


        // amount params

        $processor->setRequestedAmount($amount->getAmount()); // decimal 10, 2
        $processor->setRequestedAmountCurrency($amount->getCurrency());

        return $processor;
    }

}