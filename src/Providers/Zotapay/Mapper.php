<?php
/**
 * Created by PhpStorm.
 * User: yotam
 * Date: 11/29/17
 * Time: 6:30 PM
 */

namespace Syotams\Payment\Providers\Zotapay;


use Syotams\Payment\Contracts\Model\IPayment;
use Syotams\Payment\Providers\Zotapay\Requests\PaymentFormRequest;

class Mapper
{

    public function mapPaymentToRequest(PaymentFormRequest $request, IPayment $payment)
    {
        $amount     = $payment->getAmount();
        $customer   = $payment->getCustomer();
        $item       = $payment->getItem();


        // customer params

        $request->setFirstName($customer->getFirstName());
        $request->setLastName($customer->getLastName());
        $request->setAddress1($customer->getAddress());
        $request->setCity($customer->getCity());
        $request->setZipCode($customer->getZipCode());
        $request->setCountry($customer->getCountry());
        $request->setPhone($customer->getPhone());
        $request->setEmail($customer->getEmail());


        // item params

        $request->setOrderDesc($item->getName());


        // amount params

        $request->setAmount($amount->getAmount()); // decimal 10, 2
        $request->setCurrency($amount->getCurrency());

        return $request;
    }

}