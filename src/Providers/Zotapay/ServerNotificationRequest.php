<?php
/**
 * Created by PhpStorm.
 * User: yotam
 * Date: 12/4/17
 * Time: 7:44 PM
 */

namespace Syotams\Payment\Providers\Zotapay;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Log;

class ServerNotificationRequest extends FormRequest
{

    private $command;


    public function authorize()
    {
        Log::info('Zotapay notification received from '
            . $this->ip() . ' with status: ' . $this->get('status'));

        $env = config('payment.env');

        if(false === $this->isZotapayIP($env)) {
            Log::alert('Zotapay ip is incorrect for order uuid: ' . $this->get('client_orderid'));
            return false;
        }

        Log::info('Zotapay notification authorized for order uuid: ' . $this->get('client_orderid'));

        return true;
    }

    private function isZotapayIP($env)
    {
        if(env('APP_ENV')=='local') {
            return true;
        }

        $serverIP = config("payment.providers.zotapay.{$env}.notifications_server_ip");

        return $this->ip() === $serverIP;
    }

    public function getCommand(): NotificationCommand
    {
        if($this->command) {
            return $this->command;
        }

        $this->command = new NotificationCommand();

        $this->command->setMerchantTransactionId($this->get('client_orderid'));
        $this->command->setExpectedAmount($this->get('amount'));
        $this->command->setStatus($this->get('status'));
        $this->command->setControl($this->get('control'));
        $this->command->setProviderOrderid($this->get('orderid'));
        $this->command->setErrorCode($this->get('error_code'));
        $this->command->setErrorMessage($this->get('error_message'));

        Log::info('Zotapay NotificationCommand created for orderid: ' .  $this->command->getMerchantTransactionId());

        return $this->command;
    }

    public function rules()
    {
        return [
            'client_orderid' => 'required',
            'amount' => 'required',
            'status' => 'required',
            'control' => 'required'
        ];
    }

}