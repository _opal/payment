<?php
/**
 * Created by PhpStorm.
 * User: yotam
 * Date: 11/30/17
 * Time: 4:25 PM
 */

namespace Syotams\Payment\Providers\Zotapay\Responses;


use Syotams\Payment\Contracts\IInitialResponse;

class PaymentFormResponseAdapter implements IInitialResponse
{

    private $paymentFormResponse;

    /**
     * PaymentFormResponseAdapter constructor.
     * @param $paymentFormResponse
     */
    public function __construct(PaymentFormResponse $paymentFormResponse)
    {
        $this->paymentFormResponse = $paymentFormResponse;
    }


    public function getErrorCode()
    {
        $this->paymentFormResponse->getErrorCode();
    }

    public function getErrorMessage()
    {
        $this->paymentFormResponse->getErrorMessage();
    }

    public function getRedirectURL()
    {
        $this->paymentFormResponse->getRedirectUrl();
    }

    public function getToken()
    {
        return $this->paymentFormResponse->getSerialNumber();
    }

    public function getProviderOrderId()
    {
        $this->paymentFormResponse->getPaynetOrderId();
    }

    public function getResponse()
    {
        return $this->paymentFormResponse;
    }
}