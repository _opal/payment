<?php
/**
 * Created by PhpStorm.
 * User: yotam
 * Date: 11/30/17
 * Time: 3:38 PM
 */

namespace Syotams\Payment\Providers\Zotapay\Responses;


class PaymentFormResponse
{
    // The type of response. May be async-response, validation-error, error.
    // If type equals validation-error or error, error-message and error-code parameters contain error details.
    private $type;

    private $status;

    private $paynetOrderId;

    private $merchantOrderId;

    private $serialNumber;

    private $errorMessage;

    private $errorCode;

    private $redirect_url;


    public static function build($data)
    {
        $res = new self();

        $res->type = array_get($data, 'type');
        $res->status = array_get($data, 'status');
        $res->paynetOrderId = array_get($data, 'paynet-order-id');
        $res->merchantOrderId = array_get($data, 'merchant-order-id');
        $res->serialNumber = array_get($data, 'serial-number');
        $res->errorMessage = array_get($data, 'error-message');
        $res->errorCode = array_get($data, 'error-code');
        $res->redirect_url = array_get($data, 'redirect-url');

        return $res;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getPaynetOrderId()
    {
        return $this->paynetOrderId;
    }

    /**
     * @return mixed
     */
    public function getMerchantOrderId()
    {
        return $this->merchantOrderId;
    }

    /**
     * @return mixed
     */
    public function getSerialNumber()
    {
        return $this->serialNumber;
    }

    /**
     * @return mixed
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getRedirectUrl()
    {
        return $this->redirect_url;
    }

}