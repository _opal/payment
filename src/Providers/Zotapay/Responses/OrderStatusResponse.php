<?php
/**
 * Created by PhpStorm.
 * User: yotam
 * Date: 12/3/17
 * Time: 7:53 PM
 */

namespace Syotams\Payment\Providers\Zotapay\Responses;


class OrderStatusResponse
{
    private $type;

    private $status;

    private $amount;

    private $currency;

    private $paynet_order_id;

    private $merchant_order_id;

    private $by_request_sn;

    private $verified_3d_status;

    private $serial_number;


    /**
     * OrderStatusResponse constructor.
     * @param $data
     */
    public function __construct(array $data)
    {
        $this->type = array_get($data, 'type');
        $this->status = array_get($data, 'status');
        $this->amount = array_get($data, 'amount');
        $this->currency = array_get($data, 'currency');
        $this->paynet_order_id = array_get($data, 'paynet-order-id');
        $this->merchant_order_id = array_get($data, 'merchant-order-id');
        //$this->by_request_sn = array_get($data, 'by_request_sn');
        $this->verified_3d_status = array_get($data, 'verified-3d-status');
        $this->serial_number = array_get($data, 'serial-number');
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @return mixed
     */
    public function getPaynetOrderId()
    {
        return $this->paynet_order_id;
    }

    /**
     * @return mixed
     */
    public function getMerchantOrderId()
    {
        return $this->merchant_order_id;
    }

    /**
     * @return mixed
     */
    public function getByRequestSn()
    {
        return $this->by_request_sn;
    }

    /**
     * @return mixed
     */
    public function getVerified3dStatus()
    {
        return $this->verified_3d_status;
    }

    /**
     * @return mixed
     */
    public function getSerialNumber()
    {
        return $this->serial_number;
    }

}