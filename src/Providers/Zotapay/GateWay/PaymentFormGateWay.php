<?php
/**
 * Created by PhpStorm.
 * User: yotam
 * Date: 11/29/17
 * Time: 7:47 PM
 */

namespace Syotams\Payment\Providers\Zotapay\GateWay;


use Syotams\Payment\Contracts\IHttpRequestGateway;
use Syotams\Payment\Contracts\Model\IPayment;
use Syotams\Payment\Exception\InitialResponseException;
use Syotams\Payment\Providers\Zotapay\Mapper;
use Syotams\Payment\Providers\Zotapay\Requests\Factory;
use Syotams\Payment\Providers\Zotapay\Responses\PaymentFormResponse;

class PaymentFormGateWay
{

    private $request;
    private $httpRequestGateway;
    private $config;


    public function __construct(IHttpRequestGateway $httpRequestGateway, $config)
    {
        $this->httpRequestGateway = $httpRequestGateway;
        $this->config = $config;
    }

    /**
     * @param IPayment $payment
     * @param $transferUUID
     * @return PaymentFormResponse
     * @throws \Exception
     */
    public function post(IPayment $payment, $transferUUID)
    {
        $this->createRequest($payment, $transferUUID);

        $body = $this->httpRequestGateway->request($this->request);

        $paramsArr = null;
        parse_str($body, $paramsArr);

        return $this->createResponse($paramsArr);
    }

    public function getEndPoint()
    {
        return $this->request->getEndpointID();
    }

    private function createRequest(IPayment $payment, $transferUUID)
    {
        $this->request = (new Factory($this->config, $payment->getCustomer()->getCountry()))->create('init');

        (new Mapper())->mapPaymentToRequest($this->request, $payment);

        $this->request->setClientOrderId($transferUUID);
        $this->request->createControl();
    }

    /**
     * @param $paramsArr
     * @return PaymentFormResponse
     * @throws \Exception
     */
    private function createResponse($paramsArr): PaymentFormResponse
    {
        $response = PaymentFormResponse::build($paramsArr);

        if($response->getErrorCode() != null) {
            throw new InitialResponseException($response->getErrorMessage(), $response->getErrorCode());
        }

        return $response;
    }
}