<?php
/**
 * Created by PhpStorm.
 * User: yotam
 * Date: 12/3/17
 * Time: 7:33 PM
 */

namespace Syotams\Payment\Providers\Zotapay;


use Syotams\Payment\External\Http\TransactionResponse;

class PaymentResultResponse extends TransactionResponse
{

    private $status;

    private $orderid;

    private $client_orderid;

    private $control;

    private $descriptor;

    private $error_message;


    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getOrderid()
    {
        return $this->orderid;
    }

    /**
     * @param mixed $orderid
     */
    public function setOrderid($orderid)
    {
        $this->orderid = $orderid;
    }

    public function setClientOrderId($client_orderid)
    {
        $this->client_orderid = $client_orderid;
    }

    public function setControl($control)
    {
        $this->control = $control;
    }

    /**
     * @return mixed
     */
    public function getDescriptor()
    {
        return $this->descriptor;
    }

    /**
     * @param mixed $descriptor
     */
    public function setDescriptor($descriptor)
    {
        $this->descriptor = $descriptor;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getClientOrderid()
    {
        return $this->client_orderid;
    }

    /**
     * @return mixed
     */
    public function getControl()
    {
        return $this->control;
    }

    public function setErrorMessage($error_message)
    {
        $this->error_message = $error_message;
    }


}