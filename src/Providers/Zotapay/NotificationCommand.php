<?php
/**
 * Created by PhpStorm.
 * User: yotam
 * Date: 12/5/17
 * Time: 2:26 PM
 */

namespace Syotams\Payment\Providers\Zotapay;


use Syotams\Payment\Application\Transfers\DefaultNotificationCommand;

class NotificationCommand extends DefaultNotificationCommand
{

    private $control;

    private $error_code;

    private $error_message;

    private $provider_orderid;


    /**
     * @param mixed $control
     */
    public function setControl($control)
    {
        $this->control = $control;
    }

    /**
     * @return mixed
     */
    public function getControl()
    {
        return $this->control;
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->error_code;
    }

    /**
     * @param mixed $error_code
     */
    public function setErrorCode($error_code)
    {
        $this->error_code = $error_code;
    }

    /**
     * @return mixed
     */
    public function getErrorMessage()
    {
        return $this->error_message;
    }

    /**
     * @param mixed $error_message
     */
    public function setErrorMessage($error_message)
    {
        $this->error_message = $error_message;
    }

    /**
     * @return mixed
     */
    public function getProviderOrderid()
    {
        return $this->provider_orderid;
    }

    /**
     * @param mixed $provider_orderid
     */
    public function setProviderOrderid($provider_orderid)
    {
        $this->provider_orderid = $provider_orderid;
    }

}