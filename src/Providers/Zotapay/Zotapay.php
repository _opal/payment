<?php
/**
 * Created by PhpStorm.
 * User: yotam
 * Date: 11/29/17
 * Time: 5:58 PM
 */

namespace Syotams\Payment\Providers\Zotapay;


use Illuminate\Http\Request;
use Syotams\Payment\Application\Transfers\DefaultNotificationCommand;
use Syotams\Payment\Contracts\IHttpRequestGateway;
use Syotams\Payment\Contracts\IInitialResponse;
use Syotams\Payment\Contracts\IPaymentProvider;
use Syotams\Payment\Contracts\Model\IPayment;
use Syotams\Payment\Contracts\Model\IPaymentResult;
use Syotams\Payment\Domain\Models\Transfers\Transfer;
use Syotams\Payment\Domain\Models\Transfers\TransferStatus;
use Syotams\Payment\External\Http\TransactionResponse;
use Syotams\Payment\Providers\PaymentResult;
use Syotams\Payment\Providers\Zotapay\GateWay\PaymentFormGateWay;
use Syotams\Payment\Providers\Zotapay\Requests\Factory;
use Syotams\Payment\Providers\Zotapay\Responses\OrderStatusResponse;
use Syotams\Payment\Providers\Zotapay\Responses\PaymentFormResponseAdapter;

class Zotapay implements IPaymentProvider
{

    private $httpRequestGateway;

    private $config;


    public function __construct(IHttpRequestGateway $httpRequestGateway, $config)
    {
        $this->httpRequestGateway = $httpRequestGateway;

        $this->config = $config;
    }

    /**
     * @param IPayment $payment
     * @param Transfer $transfer
     * @return IInitialResponse
     * @throws \Exception
     */
    public function initiateRequest(IPayment $payment, Transfer $transfer): IInitialResponse
    {
        $initiator = new PaymentFormGateWay($this->httpRequestGateway, $this->config);
        $response = $initiator->post($payment, $transfer->getUUID());

        $transfer->setMetaItem('end_point', $initiator->getEndPoint());

        return new PaymentFormResponseAdapter($response);
    }

    public function getPaymentFormUrl(IInitialResponse $response)
    {
        return $response->getResponse()->getRedirectUrl();
    }

    public function createTransactionResponse(Request $response): TransactionResponse
    {
        $paymentResponse = new PaymentResultResponse(
            $response->get('client_orderid'),
            $response->get('error_message'),
            $response->get('error_code')
        );

        $paymentResponse->setStatus($response->get('status'));
        $paymentResponse->setOrderid($response->get('orderid'));
        $paymentResponse->setControl($response->get('control'));
        $paymentResponse->setDescriptor($response->get('descriptor'));

        return $paymentResponse;
    }

    /**
     * @param Transfer $transfer
     * @return IPaymentResult
     * @throws \Exception
     */
    public function verifyPayment(Transfer $transfer): IPaymentResult
    {
        $orderStatusRequest = (new Factory($this->config))->create('status');
        $orderStatusRequest->setEndpointID($transfer->getMeta('end_point'));

        $orderStatusRequest->setClientOrderid($transfer->getUUID());
        $orderStatusRequest->setOrderid($transfer->getProviderOrderId());
        $orderStatusRequest->setByRequestSn($transfer->getToken());
        $orderStatusRequest->createControl();

        $body = $this->httpRequestGateway->request($orderStatusRequest);
        $body = preg_replace("/[\r\n]*/","",$body);

        $paramsArr = null;
        parse_str($body, $paramsArr);

        $response = new OrderStatusResponse($paramsArr);

        // TODO handle $response->type = 'validation-error' or any other available type

        if(in_array($response->getStatus(), ['error', 'declined', 'filtered', 'unknown'])) {
            throw new \Exception('Transaction returned with status: ' . $response->getStatus(), 500);
        }

        return new PaymentResult(
            $response->getAmount(),
            $response->getCurrency(),
            $this->mapToTransferStatus($response->getStatus())
        );
    }

    public function verifyTransaction(Transfer $transfer, DefaultNotificationCommand $command)
    {
        if(!($command instanceof NotificationCommand)) {
            return false;
        }

        // no currency in response from zotapay so we use $transfer->getCurrency()
        return $this->verifyAmounts($transfer, $command->getExpectedAmount(), $transfer->getCurrency())
            and $command->getControl() == sha1(
                $command->getStatus()
                . $command->getProviderOrderid()
                . $command->getMerchantTransactionId()
                . $this->config['merchant_control']
            );
    }

    public function verifyAmounts(Transfer $transfer, $amount, $currency)
    {
        // It was asked by Ran not to no verify amounts
        return true; // $transfer->getCurrency() == $currency and $transfer->getAmount() == $amount;
    }

    public function mapToTransferStatus($status)
    {
        $status = strtoupper($status);

        switch ($status) {
            case 'PROCESSING':
            case 'UNKNOWN':
                return TransferStatus::PENDING;

            case 'APPROVED':
                return TransferStatus::VERIFIED;

            case 'DECLINED':
                return TransferStatus::CANCELED;

            case 'ERROR':
            case 'FILTERED':
                return TransferStatus::ERROR;

            default:
                throw new \InvalidArgumentException();
        }
    }

}