<?php
/**
 * Created by PhpStorm.
 * User: yotam
 * Date: 12/3/17
 * Time: 7:23 PM
 */

namespace Syotams\Payment\Providers\Zotapay\Requests;


class OrderStatusRequest extends AbstractRequest
{

    // merchant login name
    private $login;

    // merchant order id
    private $client_orderid;

    // zotapay order id
    private $orderid;

    //
    private $control;

    // Serial number from status request
    private $by_request_sn;

    private $merchantControl;


    /**
     * OrderStatusRequest constructor.
     * @param $url
     */
    public function __construct($url)
    {
        $this->setApiUrl($url);
    }


    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @return mixed
     */
    public function getClientOrderid()
    {
        return $this->client_orderid;
    }

    /**
     * @param mixed $client_orderid
     */
    public function setClientOrderid($client_orderid)
    {
        $this->client_orderid = $client_orderid;
    }

    /**
     * @return mixed
     */
    public function getOrderid()
    {
        return $this->orderid;
    }

    /**
     * @param mixed $orderid
     */
    public function setOrderid($orderid)
    {
        $this->orderid = $orderid;
    }

    /**
     * @return mixed
     */
    public function getControl()
    {
        return $this->control;
    }

    /**
     * @param mixed $control
     */
    public function setControl($control)
    {
        $this->control = $control;
    }

    /**
     * @return mixed
     */
    public function getByRequestSn()
    {
        return $this->by_request_sn;
    }

    /**
     * @param mixed $by_request_sn
     */
    public function setByRequestSn($by_request_sn)
    {
        $this->by_request_sn = $by_request_sn;
    }

    public function setMerchantControl($merchantControl)
    {
        $this->merchantControl = $merchantControl;
    }

    public function createControl()
    {
        $this->control = sha1(
            $this->login . $this->client_orderid . $this->orderid . $this->merchantControl
        );

        return $this->control;
    }

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return parent::toArray() + get_object_vars($this);
    }

}