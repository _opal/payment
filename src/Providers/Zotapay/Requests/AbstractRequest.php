<?php
/**
 * Created by PhpStorm.
 * User: yotam
 * Date: 11/28/17
 * Time: 6:33 PM
 */

namespace Syotams\Payment\Providers\Zotapay\Requests;


use Illuminate\Contracts\Support\Jsonable;
use JsonSerializable;
use Syotams\Payment\Contracts\IRequest;
use Illuminate\Http\Request;

class AbstractRequest implements IRequest, Jsonable, JsonSerializable
{
    private $request_method = Request::METHOD_POST;

    private $apiUrl;

    private $endpointID;

    private $headers = [];


    /**
     * @param mixed $apiUrl
     */
    public function setApiUrl($apiUrl)
    {
        $this->apiUrl = $apiUrl;
    }

    function getURL()
    {
        return $this->apiUrl . '/' . $this->endpointID;
    }

    public function setUrl($url)
    {
        $this->apiUrl = $url;
        return $this;
    }

    public function setEndpointID($endpointID)
    {
        $this->endpointID = $endpointID;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEndpointID()
    {
        return $this->endpointID;
    }

    function getRequestMethod()
    {
        return $this->request_method;
    }

    /**
     * @param string $request_method
     */
    public function setRequestMethod(string $request_method)
    {
        $this->request_method = $request_method;
    }

    public function setHeaders(array $headers)
    {
        $this->headers = $headers;
    }

    public function getHeaders()
    {
        return $this->headers;
    }


    public function toArray()
    {
        return get_object_vars($this);
    }

    /**
     * Convert the object to its JSON representation.
     *
     * @param  int $options
     * @return string
     */
    public function toJson($options = 0)
    {
        return json_encode($this->jsonSerialize(), $options);
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return $this->toArray();
    }
}