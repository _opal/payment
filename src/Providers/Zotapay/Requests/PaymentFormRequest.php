<?php
/**
 * Created by PhpStorm.
 * User: yotam
 * Date: 11/28/17
 * Time: 6:41 PM
 */

namespace Syotams\Payment\Providers\Zotapay\Requests;


use Brick\Math\BigDecimal;
use Brick\Math\RoundingMode;

class PaymentFormRequest extends AbstractRequest
{

    // our transaction id
    private $client_orderid;

    private $order_desc;


    // customer params

    private $first_name;

    private $last_name;

    private $address1;

    private $city;

    private $zip_code;

    private $country;

    private $phone;

    private $email;

    private $amount;

    private $currency;

    private $ipaddress;

    private $merchant_control;

    private $control;

    private $redirect_url;

    private $server_callback_url;


    /**
     * @return mixed
     */
    public function getClientOrderId()
    {
        return $this->client_orderid;
    }

    /**
     * @param mixed $client_orderid
     */
    public function setClientOrderId($client_orderid)
    {
        $this->client_orderid = $client_orderid;
    }

    /**
     * @return mixed
     */
    public function getOrderDesc()
    {
        return $this->order_desc;
    }

    /**
     * @param mixed $order_desc
     */
    public function setOrderDesc($order_desc)
    {
        $this->order_desc = $order_desc;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @param mixed $first_name
     */
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * @param mixed $last_name
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;
    }

    /**
     * @return mixed
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * @param mixed $address1
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getZipCode()
    {
        return $this->zip_code;
    }

    /**
     * @param mixed $zip_code
     */
    public function setZipCode($zip_code)
    {
        $this->zip_code = $zip_code;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return mixed
     */
    public function getIpaddress()
    {
        return $this->ipaddress;
    }

    /**
     * @param mixed $ipaddress
     */
    public function setIpaddress($ipaddress)
    {
        $this->ipaddress = $ipaddress;
    }

    /**
     * @return mixed
     */
    public function getControl()
    {
        return $this->control;
    }

    public function createControl()
    {
        $str = $this->getEndpointID()
            . $this->getClientOrderId()
            . BigDecimal::of($this->amount)->toScale(2, RoundingMode::DOWN)->unscaledValue()
            . $this->getEmail()
            . $this->merchant_control;

        $this->control = sha1($str);

        return $this->control;
    }

    /**
     * @param mixed $control
     */
    public function setMerchantControl($control)
    {
        $this->merchant_control = $control;
    }

    /**
     * @return mixed
     */
    public function getRedirectUrl()
    {
        return $this->redirect_url;
    }

    /**
     * @param mixed $redirect_url
     */
    public function setRedirectUrl($redirect_url)
    {
        $this->redirect_url = $redirect_url;
    }

    /**
     * @return mixed
     */
    public function getServerCallbackUrl()
    {
        return $this->server_callback_url;
    }

    /**
     * @param mixed $server_callback_url
     */
    public function setServerCallbackUrl($server_callback_url)
    {
        $this->server_callback_url = $server_callback_url;
    }



    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return parent::toArray() + get_object_vars($this);
    }

}