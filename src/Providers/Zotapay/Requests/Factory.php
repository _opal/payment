<?php
/**
 * Created by PhpStorm.
 * User: yotam
 * Date: 11/30/17
 * Time: 3:20 PM
 */

namespace Syotams\Payment\Providers\Zotapay\Requests;

use \Illuminate\Support\Facades\Request;


class Factory
{

    private $apiURL;
    private $merchantControl;
    private $redirectUrl;
    private $endpoints;
    private $statusUrl;
    private $login;
    private $serverCallbackURL;
    private $country;


    public function __construct($config, $country = null)
    {
        $this->merchantControl  = array_get($config, 'merchant_control');
        $this->apiURL           = array_get($config, 'api_url');
        $this->redirectUrl      = array_get($config, 'redirect_url');
        $this->endpoints        = array_get($config, 'end_point');
        $this->statusUrl        = array_get($config, 'status_url');
        $this->login            = array_get($config, 'login');
        $this->serverCallbackURL =  array_get($config, 'notification_url');

        $this->country = $country;
    }

    public function create($request)
    {
        switch ($request) {
            case 'init':
                return $this->createPaymentFormRequest();

            case 'status':
                return $this->createOrderStatusRequest();

            default:
                throw new \InvalidArgumentException();
        }
    }

    /**
     * @return PaymentFormRequest
     * @throws \Exception
     */
    private function createPaymentFormRequest()
    {
        $request = new PaymentFormRequest();

        $request->setApiUrl($this->apiURL);
        $request->setEndpointID($this->getEndPoint());
        $request->setRedirectUrl($this->redirectUrl);
        $request->setMerchantControl($this->merchantControl);
        $request->setServerCallbackUrl($this->serverCallbackURL);
        $request->setIpaddress(last(Request::ips()));

        $this->validateRequest($request);

        return $request;
    }

    /**
     * @param PaymentFormRequest $request
     * @throws \Exception
     */
    private function validateRequest(PaymentFormRequest $request)
    {
        $errors = [];

        if(!$request->getURL()) {
            $errors[] = 'You must set url';
        }
        if(!$request->getRedirectUrl()) {
            $errors[] = 'Callback url must be set';
        }

        if(count($errors)) {
            throw new \Exception(json_encode($errors));
        }
    }

    private function createOrderStatusRequest()
    {
        $orderStatusRequest = new OrderStatusRequest($this->statusUrl);

        $orderStatusRequest->setLogin($this->login);
        $orderStatusRequest->setEndpointID($this->getEndPoint());
        $orderStatusRequest->setMerchantControl($this->merchantControl);

        return $orderStatusRequest;
    }

    private function getEndPoint() {
        switch ($this->country) {
            case 'CN':
            case 'CHN':
                return $this->endpoints['cny'];

            default:
                return $this->endpoints['default'];
        }
    }

}