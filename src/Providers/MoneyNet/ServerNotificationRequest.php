<?php
/**
 * Created by PhpStorm.
 * User: yotam
 * Date: 2/2/17
 * Time: 5:15 PM
 */

namespace Syotams\Payment\Providers\MoneyNet;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Log;

class ServerNotificationRequest extends FormRequest
{
    private $command;


    public function authorize()
    {
        Log::info('MoneyNet notification received from '
            . $this->getClientIp()
            . ' with status: '
            . $this->get('status'));

        $env = config('payment.env');

        if(env('APP_ENV') == 'local' && $this->isPasswordOK($env)) {
            return true;
        }

        if(false === $this->isPasswordOK($env)) {
            Log::alert('MoneyNet password is incorrect');
            return false;
        }

        if(false === $this->isMNIP($env)) {
            Log::alert('MoneyNet ip is incorrect');
            return false;
        }

        Log::info('MoneyNet notification authorized');
        return true;
    }

    private function isPasswordOK($env)
    {
        $password = config("payment.providers.money_net.{$env}.notification_password");
        $requestPassword = $this->get('notification_pass');

        return $password === $requestPassword;
    }

    private function isMNIP($env)
    {
        $serverIP = config("payment.providers.money_net.{$env}.notifications_server_ip");

        return last($this->getClientIps()) === $serverIP;
    }

    public function getCommand(): NotificationCommand
    {
        if($this->command) {
            return $this->command;
        }

        $this->command = new NotificationCommand();

        $this->command->setMerchantTransactionId($this->get('merchant_transaction_id'));
        $this->command->setExpectedAmount($this->get('expected_amount'));
        $this->command->setExpectedCurrency($this->get('expected_currency'));
        $this->command->setStatus($this->get('status'));
        $this->command->setTimeInitiated($this->get('time_initiated'));
        $this->command->setTimeCompleted($this->get('time_completed'));
        $this->command->setTimeCancelled($this->get('time_cancelled'));
        $this->command->setTimeDone($this->get('time_done'));

        Log::info('MoneyNet NotificationCommand created');

        return $this->command;
    }

    public function rules()
    {
        return [
            'merchant_transaction_id' => 'required',
            'expected_amount' => 'required',
            'expected_currency' => 'required',
            'status' => 'required',
            'notification_pass' => 'required'
        ];
    }
}