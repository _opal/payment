<?php

namespace Syotams\Payment\Providers\MoneyNet;

use Illuminate\Http\Request;
use Syotams\Payment\Application\Transfers\DefaultNotificationCommand;
use Syotams\Payment\Contracts\IRequest;
use Syotams\Payment\Contracts\IHttpRequestGateway;
use Syotams\Payment\Contracts\IInitialResponse;
use Syotams\Payment\Contracts\IPaymentProvider;
use Syotams\Payment\Contracts\Model\IPayment;
use Syotams\Payment\Contracts\Model\IPaymentResult;
use Syotams\Payment\Domain\Models\Transfers\Transfer;
use Syotams\Payment\Domain\Models\Transfers\TransferStatus;
use Syotams\Payment\Exception\InitialResponseException;
use Syotams\Payment\External\Http\TransactionResponse;
use Syotams\Payment\Providers\MoneyNet\Requests\GenerateTokenRequest;
use Syotams\Payment\Providers\MoneyNet\Requests\ReportRequest;
use Syotams\Payment\Providers\MoneyNet\Responses\GenerateTokenResponse;
use Syotams\Payment\Providers\MoneyNet\Responses\GenerateTokenResponseAdapter;
use Syotams\Payment\Providers\PaymentResult;


class MoneyNet implements IPaymentProvider
{
    private $httpRequestGateway;

    private $merchant_id;

    private $merchant_name;

    private $merchant_password;

    private $services;

    private $apiURL;

    private $success_url;

    private $error_url;

    private $cancel_url;

    private $editable_transaction_details = 0;

    private $editable_customer_details = 0;


    public function __construct(IHttpRequestGateway $httpRequestGateway, $config)
    {
        $this->httpRequestGateway = $httpRequestGateway;

        $this->merchant_id       = array_get($config, 'merchant_id');
        $this->merchant_name     = array_get($config, 'merchant_name');
        $this->merchant_password = array_get($config, 'merchant_password');
        $this->apiURL            = array_get($config, 'api_url');

        $this->services     = array_get($config, 'services');
        $this->error_url    = array_get($config, 'error_url');
        $this->success_url  = array_get($config, 'success_url');
        $this->cancel_url   = array_get($config, 'cancel_url');
    }

    /**
     * @param IPayment $payment
     * @param Transfer $transfer
     * @return IInitialResponse
     * @throws \Exception
     */
    public function initiateRequest(IPayment $payment, Transfer $transfer): IInitialResponse
    {
        $request = $this->createRequest($payment, $transfer->getUUID());

        $body = $this->httpRequestGateway->request($request);

        $paramsArr = null;
        parse_str($body, $paramsArr);

        $response = new GenerateTokenResponseAdapter(new GenerateTokenResponse($paramsArr));

        if($response->getErrorCode()!=0 || $response->getResponse()->getToken()==null) {
            throw new InitialResponseException($response->getErrorMessage(), $response->getErrorCode());
        }

        return $response;
    }

    public function verifyPayment(Transfer $transfer): IPaymentResult
    {
        $request = new ReportRequest();

        $request->setUrl($this->apiURL);
        $request->setMerchantTransactionId($transfer->getUUID());
        $request->setMerchantId($this->merchant_id);
        $request->setMerchantName($this->merchant_name);
        $request->setMerchantPassword($this->merchant_password);

        $body = $this->httpRequestGateway->request($request);
        $transactions = null;

        if(isset($body)) {
            $transactions = json_decode($body);
        }

        if(!isset($transactions->MNI->transactions[0])) {
            throw new \Exception('Cannot verify transaction, empty result returned from provider API', 500);
        }

        $transaction = $transactions->MNI->transactions[0];

        return new PaymentResult(
            $transaction->expected_amount,
            $transaction->expected_currency,
            $this->mapToTransferStatus($transaction->status)
        );
    }

    public function createTransactionResponse(Request $response): TransactionResponse
    {
        return new TransactionResponse(
            $response->get('merchant_transaction_id'),
            $response->get('res_msg'),
            $response->get('res_code')
        );
    }

    private function createRequest(IPayment $payment, $transferUUID): IRequest
    {
        $request = new GenerateTokenRequest();

        (new Mapper())->mapPaymentToRequest($request, $payment);

        $request->setMerchantTransactionId($transferUUID);
        $request->setUrl($this->apiURL);
        $request->setServices($this->services);
        $request->setMerchantId($this->merchant_id);
        $request->setMerchantName($this->merchant_name);
        $request->setMerchantPassword($this->merchant_password);
        $request->setSuccessUrl($this->success_url);
        $request->setErrorUrl($this->error_url);
        $request->setCancelUrl($this->cancel_url);
        $request->setEditableCustomerDetails($this->editable_customer_details);
        $request->setEditableTransactionDetails($this->editable_transaction_details);

        $this->validateRequest($request);

        return $request;
    }

    private function validateRequest(GenerateTokenRequest $request)
    {
        $errors = [];

        if(!$request->getMerchantId() or !$request->getMerchantName() or !$request->getMerchantPassword()) {
            $errors[] = 'You must set credentials';
        }
        if(0 === count($request->getServices())) {
            $errors[] = 'At least one service must be set';
        }
        if(!$request->getErrorUrl()) {
            $errors[] = 'Error url must be set';
        }
        if(!$request->getSuccessUrl()) {
            $errors[] = 'Success url must be set';
        }
        if(!$request->getCancelUrl()) {
            $errors[] = 'Cancel url must be set';
        }

        if(count($errors)) {
            throw new \Exception(json_encode($errors));
        }
    }

    public function verifyTransaction(Transfer $transfer, DefaultNotificationCommand $command)
    {
        if(!($command instanceof NotificationCommand)) {
            return false;
        }

        return $this->verifyAmounts($transfer, $command->getExpectedAmount(), $command->getExpectedCurrency());
    }

    public function verifyAmounts(Transfer $transfer, $amount, $currency)
    {
        return true; //$transfer->getAmount() == $amount and $transfer->getCurrency() == $currency;
    }

    public function setServices($services)
    {
        $this->services = $services;
    }

    public function setSuccessUrl($success_url)
    {
        $this->success_url = $success_url;
    }

    public function setErrorUrl($error_url)
    {
        $this->error_url = $error_url;
    }

    public function setCancelUrl($cancel_url)
    {
        $this->cancel_url = $cancel_url;
    }

    public function getServices()
    {
        return $this->services;
    }

    public function getSuccessUrl()
    {
        return $this->success_url;
    }

    public function getErrorUrl()
    {
        return $this->error_url;
    }

    public function getCancelUrl()
    {
        return $this->cancel_url;
    }

    public function getEditableTransactionDetails(): int
    {
        return $this->editable_transaction_details;
    }

    public function getEditableCustomerDetails(): int
    {
        return $this->editable_customer_details;
    }

    public function getPaymentFormUrl(IInitialResponse $response)
    {
        return $this->apiURL . '?mode=process_transaction&token=' . $response->getResponse()->getToken();
    }

    public function getMerchantId()
    {
        return $this->merchant_id;
    }

    public function getMerchantName()
    {
        return $this->merchant_name;
    }

    public function getMerchantPassword()
    {
        return $this->merchant_password;
    }

    public function getApiURL()
    {
        return $this->apiURL;
    }

    public function mapToTransferStatus($code)
    {
        switch ($code) {
            case 'INITIATED':
                return TransferStatus::PENDING;

            case 'PENDING_APPROVAL':
            case 'DONE':
            case 'CONFIRMED':
            case 'COMPLETED':
                return TransferStatus::VERIFIED;

            case 'CANCELLED':
                return TransferStatus::CANCELED;

            case 'FAILED':
            case 'TERMINATED':
                return TransferStatus::ERROR;

            default:
                throw new \InvalidArgumentException();
        }
    }
}