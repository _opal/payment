<?php

namespace Syotams\Payment\Providers\MoneyNet\Responses;


class GenerateTokenResponse
{
    public $res_code;
    public $res_msg;
    public $token;

    private $body;

    public function __construct($body)
    {
        $this->setResCode(array_get($body, 'res_code'));
        $this->setResmsg(array_get($body, 'res_msg'));
        $this->setToken(array_get($body, 'token'));
    }

    public function getResCode()
    {
        return $this->res_code;
    }

    public function setResCode($res_code)
    {
        $this->res_code = $res_code;
    }

    public function getResmsg()
    {
        return $this->res_msg;
    }

    public function setResmsg($res_msg)
    {
        $this->res_msg = $res_msg;
    }

    public function getToken()
    {
        return $this->token;
    }

    public function setToken($token)
    {
        $this->token = $token;
    }

    function __get($name)
    {
        if(isset($this->body[$name])) {
            return $this->body[$name];
        }

        return null;
    }

}