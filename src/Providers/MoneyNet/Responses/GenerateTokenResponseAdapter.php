<?php
/**
 * Created by PhpStorm.
 * User: yotam
 * Date: 11/30/17
 * Time: 7:00 PM
 */

namespace Syotams\Payment\Providers\MoneyNet\Responses;


use Syotams\Payment\Contracts\IInitialResponse;

class GenerateTokenResponseAdapter implements IInitialResponse
{

    private $generateTokenResponse;

    /**
     * GenerateTokenResponseAdapter constructor.
     * @param $generateTokenResponse
     */
    public function __construct(GenerateTokenResponse $generateTokenResponse)
    {
        $this->generateTokenResponse = $generateTokenResponse;
    }


    public function getErrorCode()
    {
        return $this->generateTokenResponse->getResCode();
    }

    public function getErrorMessage()
    {
        return $this->generateTokenResponse->getResmsg();
    }

    public function getRedirectURL()
    {
        return null;
    }

    public function getToken()
    {
        return $this->generateTokenResponse->getToken();
    }

    public function getProviderOrderId()
    {
        return null;
    }

    public function getResponse()
    {
        return $this->generateTokenResponse;
    }
}