<?php
/**
 * Created by PhpStorm.
 * User: yotam
 * Date: 2/2/17
 * Time: 8:12 PM
 */

namespace Syotams\Payment\Providers\MoneyNet;


use Syotams\Payment\Application\Transfers\DefaultNotificationCommand;

class NotificationCommand extends DefaultNotificationCommand
{

    private $expected_currency;

    private $time_initiated;

    private $time_cancelled;

    private $time_completed;

    private $time_done;


    /**
     * @return mixed
     */
    public function getExpectedCurrency()
    {
        return $this->expected_currency;
    }

    /**
     * @param mixed $expected_currency
     */
    public function setExpectedCurrency($expected_currency)
    {
        $this->expected_currency = $expected_currency;
    }

    /**
     * @return mixed
     */
    public function getTimeInitiated()
    {
        return $this->time_initiated;
    }

    /**
     * @param mixed $time_initiated
     */
    public function setTimeInitiated($time_initiated)
    {
        $this->time_initiated = $time_initiated;
    }

    /**
     * @return mixed
     */
    public function getTimeCancelled()
    {
        return $this->time_cancelled;
    }

    /**
     * @param mixed $time_cancelled
     */
    public function setTimeCancelled($time_cancelled)
    {
        $this->time_cancelled = $time_cancelled;
    }

    /**
     * @return mixed
     */
    public function getTimeCompleted()
    {
        return $this->time_completed;
    }

    /**
     * @param mixed $time_completed
     */
    public function setTimeCompleted($time_completed)
    {
        $this->time_completed = $time_completed;
    }

    /**
     * @return mixed
     */
    public function getTimeDone()
    {
        return $this->time_done;
    }

    /**
     * @param mixed $time_done
     */
    public function setTimeDone($time_done)
    {
        $this->time_done = $time_done;
    }


}