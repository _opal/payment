<?php

namespace Syotams\Payment\Providers\MoneyNet;


use Syotams\Payment\Contracts\Model\IPayment;
use Syotams\Payment\Providers\MoneyNet\Requests\GenerateTokenRequest;

class Mapper
{
    public function mapPaymentToRequest(GenerateTokenRequest $request, IPayment $payment)
    {
        $amount     = $payment->getAmount();
        $customer   = $payment->getCustomer();
        $item       = $payment->getItem();

        $request->setMerchantTransactionId(null);


        // customer params

        $request->setCustomerFirstName($customer->getFirstName());
        $request->setCustomerLastName($customer->getLastName());
        $request->setCustomerEmail($customer->getEmail());
        $request->setCustomerCountry($customer->getCountry());
        $request->setCustomerResidenceCountry($customer->getResidenceCountry());


        // item params

        $request->setItemName($item->getName());
        $request->setItemDescription($item->getDescription());


        // amount params

        $request->setAmount($amount->getAmount()); // decimal 10, 2
        $request->setCurrency($amount->getCurrency());

        return $request;
    }
}