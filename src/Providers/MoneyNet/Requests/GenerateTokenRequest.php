<?php
/**
 * Created by PhpStorm.
 * User: yotam
 * Date: 1/24/17
 * Time: 3:15 PM
 */

namespace Syotams\Payment\Providers\MoneyNet\Requests;


class GenerateTokenRequest extends AbstractRequest
{

    // customer params

    private $customer_first_name;

    private $customer_last_name;

    private $customer_email;

    private $customer_country;

    private $customer_residence_country;


    // item params

    private $item_name;

    private $item_description;


    // amount params

    private $amount; // decimal 10, 2

    private $currency;


    // settings

    private $services = [];

    private $merchant_success_url;

    private $merchant_error_url;

    private $merchant_cancel_url;

    private $editable_transaction_details;

    private $editable_customer_details;


    public function setUrl($url)
    {
        return parent::setUrl($url . '?mode=generate_token');
    }

    public function setCustomerFirstName($customer_first_name)
    {
        $this->customer_first_name = $customer_first_name;
    }

    public function setCustomerLastName($customer_last_name)
    {
        $this->customer_last_name = $customer_last_name;
    }

    public function setCustomerEmail($customer_email)
    {
        $this->customer_email = $customer_email;
    }

    public function setCustomerCountry($customer_country)
    {
        $this->customer_country = $customer_country;
    }

    public function setCustomerResidenceCountry($customer_residence_country)
    {
        $this->customer_residence_country = $customer_residence_country;
    }

    public function setItemName($item_name)
    {
        $this->item_name = $item_name;
    }

    public function setItemDescription($item_description)
    {
        $this->item_description = $item_description;
    }

    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    public function setServices(array $services)
    {
        $this->services = $services;
    }

    public function setSuccessUrl($success_url)
    {
        $this->merchant_success_url = $success_url;
    }

    public function setErrorUrl($error_url)
    {
        $this->merchant_error_url = $error_url;
    }

    public function setCancelUrl($cancel_url)
    {
        $this->merchant_cancel_url = $cancel_url;
    }

    public function setEditableTransactionDetails($editable_transaction_details)
    {
        $this->editable_transaction_details = $editable_transaction_details;
    }

    public function setEditableCustomerDetails($editable_customer_details)
    {
        $this->editable_customer_details = $editable_customer_details;
    }

    public function getCustomerFirstName()
    {
        return $this->customer_first_name;
    }

    public function getCustomerLastName()
    {
        return $this->customer_last_name;
    }

    public function getCustomerEmail()
    {
        return $this->customer_email;
    }

    public function getCustomerCountry()
    {
        return $this->customer_country;
    }

    public function getCustomerResidenceCountry()
    {
        return $this->customer_residence_country;
    }

    public function getItemName()
    {
        return $this->item_name;
    }

    public function getItemDescription()
    {
        return $this->item_description;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function getCurrency()
    {
        return $this->currency;
    }

    public function getServices(): array
    {
        return $this->services;
    }

    public function getSuccessUrl()
    {
        return $this->merchant_success_url;
    }

    public function getErrorUrl()
    {
        return $this->merchant_error_url;
    }

    public function getCancelUrl()
    {
        return $this->merchant_cancel_url;
    }

    /**
     * @return mixed
     */
    public function getEditableTransactionDetails()
    {
        return $this->editable_transaction_details;
    }

    /**
     * @return mixed
     */
    public function getEditableCustomerDetails()
    {
        return $this->editable_customer_details;
    }

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return parent::toArray() + get_object_vars($this);
    }

}