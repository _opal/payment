<?php
/**
 * Created by PhpStorm.
 * User: yotam
 * Date: 1/30/17
 * Time: 11:20 AM
 */

namespace Syotams\Payment\Providers\MoneyNet\Requests;


use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Http\Request;
use JsonSerializable;
use Syotams\Payment\Contracts\IRequest;

abstract class AbstractRequest implements IRequest, Jsonable, JsonSerializable
{
    private $request_method = Request::METHOD_POST;

    private $url;

    private $merchant_transaction_id;

    private $headers;

    // auth
    private $merchant_id;

    private $merchant_name;

    private $merchant_password;


    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     * @return AbstractRequest
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMerchantTransactionId()
    {
        return $this->merchant_transaction_id;
    }

    /**
     * @param mixed $merchant_transaction_id
     * @return AbstractRequest
     */
    public function setMerchantTransactionId($merchant_transaction_id)
    {
        $this->merchant_transaction_id = $merchant_transaction_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMerchantId()
    {
        return $this->merchant_id;
    }

    /**
     * @param mixed $merchant_id
     * @return AbstractRequest
     */
    public function setMerchantId($merchant_id)
    {
        $this->merchant_id = $merchant_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMerchantName()
    {
        return $this->merchant_name;
    }

    /**
     * @param mixed $merchant_name
     * @return AbstractRequest
     */
    public function setMerchantName($merchant_name)
    {
        $this->merchant_name = $merchant_name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMerchantPassword()
    {
        return $this->merchant_password;
    }

    /**
     * @param mixed $merchant_password
     * @return AbstractRequest
     */
    public function setMerchantPassword($merchant_password)
    {
        $this->merchant_password = $merchant_password;
        return $this;
    }

    /**
     * @return string
     */
    public function getRequestMethod(): string
    {
        return $this->request_method;
    }

    /**
     * @param string $request_method
     */
    public function setRequestMethod(string $request_method)
    {
        $this->request_method = $request_method;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @param array $headers
     */
    public function setHeaders(array $headers)
    {
        $this->headers = $headers;
    }

    public function toArray()
    {
        return get_object_vars($this);
    }

    /**
     * Convert the object to its JSON representation.
     *
     * @param  int $options
     * @return string
     */
    public function toJson($options = 0)
    {
        return json_encode($this->jsonSerialize(), $options);
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return $this->toArray();
    }
}