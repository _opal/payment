<?php
/**
 * Created by PhpStorm.
 * User: yotam
 * Date: 1/30/17
 * Time: 11:19 AM
 */

namespace Syotams\Payment\Providers\MoneyNet\Requests;


class ReportRequest extends AbstractRequest {

    private $mni_transaction_id;

    private $from_date;

    private $to_date;

    private $status;

    private $report_type = 'by_merchant_transaction_id';



    public function setUrl($url)
    {
        return parent::setUrl($url . '?mode=get_reports');
    }

    public function getMniTransactionId()
    {
        return $this->mni_transaction_id;
    }

    public function setMniTransactionId($mni_transaction_id)
    {
        $this->mni_transaction_id = $mni_transaction_id;
    }

    public function getFromDate()
    {
        return $this->from_date;
    }

    public function setFromDate($from_date)
    {
        $this->from_date = $from_date;
    }

    public function getToDate()
    {
        return $this->to_date;
    }

    public function setToDate($to_date)
    {
        $this->to_date = $to_date;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getReportType()
    {
        return $this->report_type;
    }

    public function setReportType($report_type)
    {
        $this->report_type = $report_type;
    }

    public function toArray()
    {
        return parent::toArray() + get_object_vars($this);
    }

}