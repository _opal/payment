<?php

namespace Syotams\Payment\External\Gateway;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Log;
use Syotams\Payment\Contracts\IRequest;
use Syotams\Payment\Contracts\IHttpRequestGateway;

class HttpRequestGateway implements IHttpRequestGateway
{

    /**
     * @param IRequest $request
     * @return string
     * @throws HttpRequestException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function request(IRequest $request) : string
    {
        $client = new Client();

        $options = [
            'form_params' => $request->toArray(),
            'verify' => false,
            'connect_timeout' => 10
        ];

        if($headers = $request->getHeaders()) {
            $options['headers'] = $headers;
        }

        try {
            $res = $client->request($request->getRequestMethod(), $request->getUrl(), $options);
        }
        catch (RequestException $e) {
            Log::error($e->getMessage());

            $error = 'An error occurred';

            if ($e->hasResponse()) {
                $error = $e->getResponse()->getBody()->getContents();
            }

            throw new HttpRequestException($error, $e->getCode());
        }

        $statusCode = $res->getStatusCode();

        if($statusCode==200) {
            return $res->getBody()->getContents();
        }
        else {
            throw new HttpRequestException('Error!', $res->getStatusCode());
        }
    }

}