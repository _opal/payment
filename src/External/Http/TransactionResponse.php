<?php

namespace Syotams\Payment\External\Http;


class TransactionResponse
{
    private $transferUUID;
    private $message;
    private $code;


    public function __construct($transferUUID, $message, $code)
    {
        $this->transferUUID = $transferUUID;
        $this->message = $message;
        $this->code = $code;
    }

    public function getTransferUUID()
    {
        return $this->transferUUID;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function getCode()
    {
        return $this->code;
    }

}