<?php
namespace Syotams\Payment;


use Illuminate\Support\Facades\Facade;

class PaymentFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'payment';
    }

    public static function MoneyNet() {
        return app('payment', ['provider' => 'money_net']);
    }

    public static function ZotaPay() {
        return app('payment', ['provider' => 'zotapay']);
    }

    public static function PayEasy() {
        return app('payment', ['provider' => 'payeasy']);
    }
}