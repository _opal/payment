<?php

namespace Syotams\Payment\Domain\Services\Factory;

use Syotams\Payment\Contracts\Model\IPayment;
use Syotams\Payment\Domain\Models\Amount;
use Syotams\Payment\Domain\Models\Customer;
use Syotams\Payment\Domain\Models\Item;
use Syotams\Payment\Domain\Models\Payment;

class PaymentBuilder
{
    private $transactionId;

    private $amount;
    private $currency;

    private $customerId;
    private $first_name;
    private $last_name;
    private $email;
    private $country;

    private $itemName;
    private $itemDesc;


    public function withCustomerId($customerId)
    {
        $this->customerId = $customerId;
    }

    public function withAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    public function withCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }

    public function withFirstName($firstName)
    {
        $this->first_name = $firstName;
        return $this;
    }

    public function withLastName($lastName)
	{
        $this->last_name = $lastName;
        return $this;
    }

    public function withCountry($country)
	{
        $this->country = $country;
        return $this;
    }

    public function withTransactionId($transactionId)
	{
        $this->transactionId = $transactionId;
        return $this;
    }

    public function withEmail($email)
	{
        $this->email = $email;
        return $this;
    }

    public function withItemName($itemName)
	{
        $this->itemName = $itemName;
        return $this;
    }

    public function withItemDescription($itemDesc)
	{
        $this->itemDesc = $itemDesc;
        return $this;
    }

    public function build() : IPayment
    {
        return new Payment(
            new Item($this->itemName, $this->itemDesc),
            new Amount($this->amount, $this->currency),
            new Customer(
                $this->customerId,
                $this->first_name,
                $this->last_name,
                $this->email,
                $this->country,
                $this->country
            )
        );
    }
}