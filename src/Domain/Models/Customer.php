<?php

namespace Syotams\Payment\Domain\Models;


class Customer
{
    private $id;

    // map to customer_first_name
    private $first_name;

    // map to customer_last_name
    private $last_name;

    // map to customer_email
    private $email;

    // map to customer_country
    private $country;

    // map to customer_residence_country
    private $residence_country;

    private $address;

    private $city;

    private $phone;

    private $zipCode;


    public function __construct($id, $first_name, $last_name, $email, $country, $residence_country)
    {
        $this->id = $id;
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->email = $email;
        $this->country = $country;
        $this->residence_country = $residence_country;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getFirstName()
    {
        return $this->first_name;
    }

    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;
    }

    public function getLastName()
    {
        return $this->last_name;
    }

    public function setLastName($last_name)
    {
        $this->last_name = $last_name;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function setCountry($country)
    {
        $this->country = $country;
    }

    public function getResidenceCountry()
    {
        return $this->residence_country;
    }

    public function setResidenceCountry($residence_country)
    {
        $this->residence_country = $residence_country;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function setAddress($address)
    {
        $this->address = $address;
    }

    public function setCity($city)
    {
        $this->city = $city;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return mixed
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

}