<?php

namespace Syotams\Payment\Domain\Models;

use Brick\Math\BigDecimal;
use Brick\Math\RoundingMode;


class Amount
{
    // decimal 10, 2
    private $amount;

    private $currency;


    public function __construct($amount, $currency)
    {
        $this->amount = $amount;
        $this->currency = $currency;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function setAmount($amount)
    {
        if($amount instanceof BigDecimal) {
            $amount = BigDecimal::of($amount)->toScale(2, RoundingMode::FLOOR);
        }

        $this->amount = $amount;
    }

    public function getCurrency()
    {
        return $this->currency;
    }

    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }


}