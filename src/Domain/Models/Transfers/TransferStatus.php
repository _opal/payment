<?php

namespace Syotams\Payment\Domain\Models\Transfers;


abstract class TransferStatus
{
    const INITIATED   = 'initiated';    // first step created
    const PENDING   = 'pending';    // customer is directed to provider, waiting for results from provider
    const SUCCESS   = 'success';    // provider returned success
    const CANCELED  = 'canceled';   // provider returned cancel
    const DECLINED = 'declined';
    const ERROR     = 'error';      // provider returned error
    const VERIFIED  = 'verified';   // payment is verified and is ok
    const COMPLETED  = 'completed'; // customer account balance was credited
}