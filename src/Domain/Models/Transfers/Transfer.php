<?php

namespace Syotams\Payment\Domain\Models\Transfers;

use Illuminate\Database\Eloquent\Model;

class Transfer extends Model
{

    public $table = 'transfers';

    protected $primaryKey = 'uuid';

    public $incrementing = false;

    protected $fillable = [
        'user_id',
        'amount',
        'currency',
        'meta'
    ];

    protected $casts = [
        'meta' => 'array'
    ];


    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->createUUID();
        $this->setStatusInitiated();
    }

    public function createUUID()
    {
        $this->uuid = md5(uniqid("user_{$this->user_id}"));
    }

    /**
     * @param $status
     * @throws InvalidStateException
     */
    public function setStatus($status)
    {
        if($this->getStatus()==$status) {
            return;
        }
        if($this->isCompleted())
        {
            throw new InvalidStateException('Status could not be updated for it is already completed');
        }
        if($this->isVerified() && $status !== TransferStatus::COMPLETED)
        {
            throw new InvalidStateException('Status could not be updated for it is already verified');
        }
        if($this->isError() or $this->isCanceled()) {
            throw new InvalidStateException('Status could not be updated for it is already canceled or has error');
        }

        $this->attributes['status'] = $status;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getResponseStatusDescription()
    {
        return $this->attributes['res_status_description'];
    }

    public function setResponseStatusDescription($description)
    {
        $this->attributes['res_status_description'] = $description;
    }

    public function setResponseStatusCode($code)
    {
        $this->res_status_code = $code;
    }

    public function getResponseStatusCode()
    {
        return $this->res_status_code;
    }

    public function getUUID()
    {
        return $this->uuid;
    }

    public function getUserId()
    {
        return $this->user_id;
    }

    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function getCurrency()
    {
        return $this->currency;
    }

    public function setCreditedAmount($credited_amount)
    {
        $this->credited_amount = $credited_amount;
    }

    public function setCreditedCurrency($credited_currency)
    {
        $this->credited_currency = $credited_currency;
    }

    public function getProviderOrderId()
    {
        return $this->attributes['provider_order_id'];
    }

    public function setProviderOrderId($id)
    {
        $this->attributes['provider_order_id'] = $id;
    }

    public function getToken()
    {
        return $this->attributes['provider_token'];
    }

    public function setToken($token)
    {
        $this->attributes['provider_token'] = $token;
    }


    public function isVerified()
    {
        return $this->getStatus() == TransferStatus::VERIFIED;
    }

    public function setStatusVerified()
    {
        $this->setStatus(TransferStatus::VERIFIED);
    }

    public function isSuccess()
    {
        return $this->getStatus() == TransferStatus::SUCCESS;
    }

    public function setStatusSuccess()
    {
        $this->setStatus(TransferStatus::SUCCESS);
    }

    public function isCompleted()
    {
        return $this->getStatus() == TransferStatus::COMPLETED;
    }

    public function setStatusCompleted()
    {
        $this->setStatus(TransferStatus::COMPLETED);
    }

    public function isCanceled()
    {
        return $this->getStatus() == TransferStatus::CANCELED;
    }

    public function setStatusCanceled()
    {
        $this->setStatus(TransferStatus::CANCELED);
    }

    public function isPending()
    {
        return $this->getStatus() == TransferStatus::PENDING;
    }

    public function setStatusPending()
    {
        $this->setStatus(TransferStatus::PENDING);
    }

    public function isError()
    {
        return $this->getStatus() == TransferStatus::ERROR;
    }

    public function setStatusError()
    {
        $this->setStatus(TransferStatus::ERROR);
    }

    public function isInitiated()
    {
        return $this->getStatus() == TransferStatus::INITIATED;
    }

    public function setStatusInitiated()
    {
        $this->setStatus(TransferStatus::INITIATED);
    }

    public static function make($customerId, $amount, $currency): Transfer
    {
        $transfer = new Transfer([
            'user_id'   => $customerId,
            'amount'    => $amount,
            'currency'    => $currency
        ]);

        if(!$transfer->save()) {
            throw new \Exception('Could not create transfer');
        }

        return $transfer;
    }

    public function setMetaItem($key, $value)
    {
        $meta = $this->getMeta();
        $meta[$key] = $value;
        $this->setMeta($meta);
    }

    // CREATE ADDRESS CASTED TO REAL OBJECT
    public function setMeta($meta)
    {
        $this->attributes['meta'] = $this->asJson($meta);
    }

    public function getMeta($key = null)
    {
        if($key) {
            return $this->meta[$key];
        }

        return $this->meta;
    }

    // we have to use attribute so this attribute will be save in db
    public function setMetaAttribute($meta)
    {
        $this->setMeta($meta);
    }
}