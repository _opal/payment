<?php

namespace Syotams\Payment\Domain\Models;

use Syotams\Payment\Contracts\Model\IPayment;

class Payment implements IPayment
{

    private $item;

    private $amount;

    private $customer;


    public function __construct($item, $amount, $customer)
    {
        $this->item = $item;
        $this->amount = $amount;
        $this->customer = $customer;
    }

    public function getItem() : Item
    {
        return $this->item;
    }

    public function setItem(Item $item)
    {
        $this->item = $item;
    }

    public function getAmount() : Amount
    {
        return $this->amount;
    }

    public function setAmount(Amount $amount)
    {
        $this->amount = $amount;
    }

    public function getCustomer() : Customer
    {
        return $this->customer;
    }

    public function setCustomer(Customer $customer)
    {
        $this->customer = $customer;
    }


    
}