<?php

namespace Syotams\Payment\Contracts\Model;
use Syotams\Payment\Domain\Models\Amount;
use Syotams\Payment\Domain\Models\Customer;
use Syotams\Payment\Domain\Models\Item;


interface IPayment
{
    public function getItem() : Item;

    public function setItem(Item $item);

    public function getAmount() : Amount;

    public function setAmount(Amount $amount);

    public function getCustomer() : Customer;

    public function setCustomer(Customer $customer);
}