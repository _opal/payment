<?php
/**
 * Created by PhpStorm.
 * User: yotam
 * Date: 1/30/17
 * Time: 3:34 PM
 */

namespace Syotams\Payment\Contracts\Model;


interface IPaymentResult
{
    function getAmount();
    function getCurrency();
    function getStatus();
}