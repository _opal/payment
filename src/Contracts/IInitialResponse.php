<?php
/**
 * Created by PhpStorm.
 * User: yotam
 * Date: 11/30/17
 * Time: 4:20 PM
 */

namespace Syotams\Payment\Contracts;


interface IInitialResponse
{

    public function getErrorCode();

    public function getErrorMessage();

    public function getToken();

    public function getProviderOrderId();

    public function getResponse();

}