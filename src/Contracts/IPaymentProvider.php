<?php

namespace Syotams\Payment\Contracts;

use Illuminate\Http\Request;
use Syotams\Payment\Application\Transfers\DefaultNotificationCommand;
use Syotams\Payment\Contracts\Model\IPayment;
use Syotams\Payment\Contracts\Model\IPaymentResult;
use Syotams\Payment\Domain\Models\Transfers\Transfer;
use Syotams\Payment\External\Http\TransactionResponse;

interface IPaymentProvider
{
    public function initiateRequest(IPayment $payment, Transfer $transfer): IInitialResponse;

    public function getPaymentFormUrl(IInitialResponse $response);

    public function createTransactionResponse(Request $response): TransactionResponse;

    public function verifyPayment(Transfer $transfer): IPaymentResult;

    public function mapToTransferStatus($code);

    public function verifyTransaction(Transfer $transfer, DefaultNotificationCommand $command);

    public function verifyAmounts(Transfer $transfer, $amount, $currency);
}