<?php

namespace Syotams\Payment\Contracts;


use Illuminate\Http\Request;
use Syotams\Payment\Contracts\Model\IPayment;

interface IPaymentService
{
    function setProvider(IPaymentProvider $provider);

    function postRequest(IPayment $payment);

    public function handleError(Request $request);

    public function handleSuccess(Request $response);

    public function handleCanceled(Request $response);
}