<?php

namespace Syotams\Payment\Contracts;


interface IHttpRequestGateway
{
    function request(IRequest $request) : string;
}