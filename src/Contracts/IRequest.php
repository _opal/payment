<?php

namespace Syotams\Payment\Contracts;

use Illuminate\Contracts\Support\Arrayable;

interface IRequest extends Arrayable
{
    public function getURL();

    public function getRequestMethod();

    public function getHeaders();
}