<?php
/**
 * Created by PhpStorm.
 * User: yotam
 * Date: 7/20/17
 * Time: 4:19 PM
 */

namespace Syotams\Payment\Application\Transfers;


use Syotams\Payment\Domain\Models\Amount;
use Syotams\Payment\Domain\Models\Customer;
use Syotams\Payment\Domain\Models\Item;
use Syotams\Payment\Domain\Models\Payment;

class ItemPaymentFacade
{
    // Item
    private $item_name;

    // item_description
    private $item_description;

    // decimal 10, 2
    private $amount;

    private $currency;

    // Customer
    private $customer_id;

    // map to customer_first_name
    private $first_name;

    // map to customer_last_name
    private $last_name;

    // map to customer_email
    private $email;

    // map to customer_country
    private $country;

    // map to customer_residence_country
    private $residence_country;

    private $address;

    private $city;

    private $zip_code;

    private $phone;


    /**
     * @return mixed
     */
    public function getItemName()
    {
        return $this->item_name;
    }

    /**
     * @param mixed $item_name
     */
    public function setItemName($item_name)
    {
        $this->item_name = $item_name;
    }

    /**
     * @return mixed
     */
    public function getItemDescription()
    {
        return $this->item_description;
    }

    /**
     * @param mixed $item_description
     */
    public function setItemDescription($item_description)
    {
        $this->item_description = $item_description;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return mixed
     */
    public function getCustomerId()
    {
        return $this->customer_id;
    }

    /**
     * @param mixed $customer_id
     */
    public function setCustomerId($customer_id)
    {
        $this->customer_id = $customer_id;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @param mixed $first_name
     */
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * @param mixed $last_name
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getResidenceCountry()
    {
        return $this->residence_country;
    }

    /**
     * @param mixed $residence_country
     */
    public function setResidenceCountry($residence_country)
    {
        $this->residence_country = $residence_country;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getZipCode()
    {
        return $this->zip_code;
    }

    /**
     * @param mixed $zip_code
     */
    public function setZipCode($zip_code)
    {
        $this->zip_code = $zip_code;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }


    public function createPayment()
    {
        $item = new Item($this->getItemName(), $this->getItemDescription());

        $amount = new Amount($this->getAmount(), $this->getCurrency());

        $customer = new Customer(
            $this->getCustomerId(),
            $this->getFirstName(),
            $this->getLastName(),
            $this->getEmail(),
            $this->getCountry(),
            $this->getResidenceCountry());

        $customer->setAddress($this->getAddress());
        $customer->setCity($this->getCity());
        $customer->setPhone($this->getPhone());
        $customer->setZipCode($this->getZipCode());

        return new Payment($item, $amount, $customer);
    }

}