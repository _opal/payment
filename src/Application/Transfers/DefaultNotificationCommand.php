<?php
/**
 * Created by PhpStorm.
 * User: yotam
 * Date: 12/5/17
 * Time: 2:18 PM
 */

namespace Syotams\Payment\Application\Transfers;


class DefaultNotificationCommand
{
    private $merchant_transaction_id;

    private $expected_amount;

    private $status;


    /**
     * @return mixed
     */
    public function getMerchantTransactionId()
    {
        return $this->merchant_transaction_id;
    }

    /**
     * @param mixed $merchant_transaction_id
     */
    public function setMerchantTransactionId($merchant_transaction_id)
    {
        $this->merchant_transaction_id = $merchant_transaction_id;
    }

    /**
     * @return mixed
     */
    public function getExpectedAmount()
    {
        return $this->expected_amount;
    }

    /**
     * @param mixed $expected_amount
     */
    public function setExpectedAmount($expected_amount)
    {
        $this->expected_amount = $expected_amount;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }
}