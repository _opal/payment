<?php

namespace Syotams\Payment\Application\Transfers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Syotams\Payment\Contracts\IInitialResponse;
use Syotams\Payment\Contracts\IPaymentProvider;
use Syotams\Payment\Contracts\IPaymentService;
use Syotams\Payment\Contracts\Model\IPayment;
use Syotams\Payment\Domain\Models\Payment;
use Syotams\Payment\Domain\Models\Transfers\Transfer;
use Syotams\Payment\Domain\Models\Transfers\TransferStatus;
use Syotams\Payment\External\Http\TransactionResponse;

class PaymentApplicationService implements IPaymentService
{
    private $provider;


    public function __construct(IPaymentProvider $provider)
    {
        $this->provider = $provider;
    }

    public function setProvider(IPaymentProvider $provider)
    {
        $this->provider = $provider;
    }

    /**
     * @param IPayment $payment
     * @return IInitialResponse
     * @throws \Exception
     * @throws \Syotams\Payment\Domain\Models\Transfers\InvalidStateException
     */
    public function postRequest(IPayment $payment): IInitialResponse
    {
        $transfer = Transfer::make(
            $payment->getCustomer()->getId(),
            $payment->getAmount()->getAmount(),
            $payment->getAmount()->getCurrency()
        );

        $response = null;

        try {
            $response = $this->provider->initiateRequest($payment, $transfer);

            $transfer->setProviderOrderId($response->getProviderOrderId());
            $transfer->setToken($response->getToken());

            $this->updateTransfer(
                $transfer,
                $response->getErrorCode(),
                $response->getErrorMessage(),
                TransferStatus::PENDING
            );
        }
        catch (\Exception $e) {
            $this->updateTransfer(
                $transfer,
                $e->getCode(),
                $e->getMessage(),
                TransferStatus::ERROR
            );

            throw $e;
        }

        return $response;
    }

    /**
     * @param Transfer $transfer
     * @param $code
     * @param $message
     * @param $status
     * @return bool
     * @throws \Syotams\Payment\Domain\Models\Transfers\InvalidStateException
     */
    private function updateTransfer(Transfer $transfer, $code, $message, $status)
    {
        $transfer->setResponseStatusCode($code);
        $transfer->setResponseStatusDescription($message);
        $transfer->setStatus($status);

        return $transfer->save();
    }

    /**
     * @param TransactionResponse $transactionResponse
     * @param $status
     * @return Transfer
     * @throws ModelNotFoundException
     * @throws \Syotams\Payment\Domain\Models\Transfers\InvalidStateException
     */
    public function applyTransactionResponse(TransactionResponse $transactionResponse, $status): Transfer {
        $transfer = $this->fetch($transactionResponse->getTransferUUID());

        if(!$transfer) {
            throw new ModelNotFoundException('Payment request does not exists in db', 5001);
        }

        $this->updateTransfer(
            $transfer,
            $transactionResponse->getCode(),
            $transactionResponse->getMessage(),
            $status
        );

        return $transfer;
    }

    /**
     * @param Payment $payment
     * @return mixed
     * @throws \Exception
     * @throws \Syotams\Payment\Domain\Models\Transfers\InvalidStateException
     */
    public function getPaymentPageUrl(Payment $payment)
    {
        return $this->provider->getPaymentFormUrl($this->postRequest($payment));
    }

    /**
     * @param Request $request
     * @return Transfer
     * @throws ModelNotFoundException
     * @throws \Syotams\Payment\Domain\Models\Transfers\InvalidStateException
     */
    public function handleError(Request $request): Transfer
    {
        return $this->applyTransactionResponse(
            $this->provider->createTransactionResponse($request),
            TransferStatus::ERROR
        );
    }

    /**
     * @param Request $request
     * @return Transfer
     * @throws ModelNotFoundException
     * @throws \Exception
     * @throws \Syotams\Payment\Domain\Models\Transfers\InvalidStateException
     * TODO: change Request to Request + Command
     * where command is retrieved from request like it does from ServerNotificationRequest
     */
    public function handleSuccess(Request $request): Transfer
    {
        Log::info('PaymentFacade: start handling success payment response');

        $response = $this->provider->createTransactionResponse($request);

        $transfer = $this->fetch($response->getTransferUUID());

        try {
            Log::info('PaymentFacade: verifying payment');

            $paymentResult = $this->provider->verifyPayment($transfer);

            Log::info('PaymentFacade: verifying amounts');

            if($this->provider->verifyAmounts($transfer, $paymentResult->getAmount(), $paymentResult->getCurrency())) {
                Log::info('PaymentFacade: verifying payment success, status: ' . $paymentResult->getStatus());

                $transfer->setCreditedAmount($paymentResult->getAmount());
                $transfer->setCreditedCurrency($paymentResult->getCurrency());

                $transfer->setStatus($paymentResult->getStatus());
            }
            else {
                Log::info(
                    'PaymentFacade: Expected amount do not fit,'
                    . " expected: {$transfer->getAmount()} , actual: {$paymentResult->getAmount()}");

                throw new \Exception('Expected amount do not fit', 5002);
            }
        }
        catch (\Exception $e) {
            Log::info('PaymentFacade: verifying payment error: ' . $e->getMessage());

            $transfer->setResponseStatusCode($e->getCode());
            $transfer->setResponseStatusDescription($e->getMessage());
            $transfer->setStatusError();

            $transfer->save();

            throw $e;
        }

        $transfer->save();

        return $transfer;
    }

    /**
     * @param Request $request
     * @return Transfer
     * @throws ModelNotFoundException
     * @throws \Exception
     * @throws \Syotams\Payment\Domain\Models\Transfers\InvalidStateException
     */
    public function handleCanceled(Request $request): Transfer
    {
        return $this->applyTransactionResponse(
            $this->provider->createTransactionResponse($request),
            TransferStatus::CANCELED
        );
    }

    /**
     * @param DefaultNotificationCommand $command
     * @return null
     * @throws \Syotams\Payment\Domain\Models\Transfers\InvalidStateException
     */
    public function handleNotification(DefaultNotificationCommand $command)
    {
        $status = null;

        try {
            $status = $this->provider->mapToTransferStatus($command->getStatus());
        }
        catch (\InvalidArgumentException $e) {
            Log::alert($e->getMessage());
            return null;
        }

        $transfer = $this->fetch($command->getMerchantTransactionId());

        if($transfer && $this->provider->verifyTransaction($transfer, $command)) {
            if($this->updateTransfer($transfer, 0, $command->getStatus(), $status)) {
                return $transfer;
            }
        }

        return null;
    }

    public function complete(Transfer $transfer)
    {
        $transfer->setStatusCompleted();
        return $transfer->save();
    }

    private function fetch($uuid)
    {
        return Transfer::where('uuid', $uuid)->lockForUpdate()->first();
    }
}