<?php

namespace Syotams\Payment;

use Illuminate\Foundation\Application as LaravelApplication;
use Illuminate\Support\ServiceProvider;
use Syotams\Payment\Application\Transfers\PaymentApplicationService;
use Syotams\Payment\Contracts\IHttpRequestGateway;
use Syotams\Payment\Contracts\IPaymentService;
use Syotams\Payment\External\Gateway\HttpRequestGateway;
use Syotams\Payment\Providers\MockProvider\MockProvider;
use Syotams\Payment\Providers\MoneyNet\MoneyNet;
use Syotams\Payment\Providers\PayEasy\PayEasy;
use Syotams\Payment\Providers\Zotapay\Zotapay;


class PaymentServiceProvider extends ServiceProvider
{
    const VERSION = '0.0.1';

    protected $defer = true;


    public function boot()
    {
        $this->publishConfig();

        $this->publishCore();
    }

    public function register()
    {
        $this->app->singleton(IHttpRequestGateway::class, function () {
            return new HttpRequestGateway();
        });

        $this->app->singleton('payment', function ($app, $params = []) {
            $config = $app->make('config')->get('payment');

            $provider   = array_get($params, 'provider', $config['provider']);
            $env        = $config['env'];
            $providerConfig = $config['providers'][$provider][$env];

            switch ($provider) {
                case 'mock':
                    $providerObject = new MockProvider();
                    break;
                case 'money_net':
                    $providerObject = new MoneyNet($this->app[IHttpRequestGateway::class], $providerConfig);
                break;
                case 'zotapay':
                    $providerObject = new Zotapay($this->app[IHttpRequestGateway::class], $providerConfig);
                    break;
                case 'payeasy':
                    $providerObject = new PayEasy($this->app[IHttpRequestGateway::class], $providerConfig);
                    break;
                default:
                    $providerObject = $this->tryFromConfigClass($providerConfig);
                    break;
            }

            return new PaymentApplicationService($providerObject);
        });

        $this->app->alias('payment', IPaymentService::class);
    }

    private function publishConfig()
    {
        $source = realpath(__DIR__ . '/../config/payment.php');

        if ($this->app instanceof LaravelApplication && $this->app->runningInConsole()) {
            $this->publishes([$source => config_path('payment.php')]);
        }

        $this->mergeConfigFrom($source, 'payment');
    }

    private function publishCore() {
        $this->publishes([
            __DIR__ . '/../migrations/2017_01_26_132952_create_transfers_table.php' =>
                base_path('database/migrations/2017_01_26_132952_create_transfers_table.php'),
        ]);
    }

    private function tryFromConfigClass($providerConfig)
    {
        $className = array_get($providerConfig, 'class', null);

        if($className) {
            return new $className();
        }

        return null;
    }

    public function provides()
    {
        return ['payment'];
    }

}