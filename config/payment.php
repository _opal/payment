<?php
/**
 * Created by PhpStorm.
 * User: yotam
 * Date: 1/24/17
 * Time: 2:46 PM
 */

return [

    'provider' => 'zotapay',

    'env' => env('PAYMENT_ENV'),

    'providers' => [

        'money_net' => [

            'sandbox' => [
                'api_url' => env('MONEY_NET_SANDBOX_URL'),
                'merchant_id' => env('MONEY_NET_SANDBOX_MERCHANT_ID'),
                'merchant_name' => env('MONEY_NET_SANDBOX_NAME'),
                'merchant_password' => env('MONEY_NET_SANDBOX_PASSWORD'),
                'notification_password' => env('MONEY_NET_SANDBOX_NOTIFICATION_PASS'),
                'success_url' => env('MONEY_NET_SUCCESS_URL'),
                'error_url' => env('MONEY_NET_ERROR_URL'),
                'cancel_url' => env('MONEY_NET_CANCEL_URL'),
                'notifications_server_ip' => env('MONEY_NET_SANDBOX_NOTIFICATION_IP'),
                'editable_transaction_details' => 0,
                'editable_customer_details' => 0,
                'services' => [4],
            ],

            'production' => [
                'api_url' => env('MONEY_NET_PRODUCTION_URL'),
                'merchant_id' => env('MONEY_NET_PRODUCTION_MERCHANT_ID'),
                'merchant_name' => env('MONEY_NET_PRODUCTION_NAME'),
                'merchant_password' => env('MONEY_NET_PRODUCTION_PASSWORD'),
                'notification_password' => env('MONEY_NET_PRODUCTION_NOTIFICATION_PASS'),
                'success_url' => env('MONEY_NET_SUCCESS_URL'),
                'error_url' => env('MONEY_NET_ERROR_URL'),
                'cancel_url' => env('MONEY_NET_CANCEL_URL'),
                'notifications_server_ip' => env('MONEY_NET_PRODUCTION_NOTIFICATION_IP'),
                'editable_transaction_details' => 0,
                'editable_customer_details' => 0,
                'services' => [4],
            ]
        ],

        'mock' => [

            'sandbox' => [
                'success_url' => 'http://localhost:8000/payments/success',
                'error_url' => 'http://localhost:8000/payments/error',
                'cancel_url' => 'http://localhost:8000/payments/cancel'
            ]

        ],

        'zotapay' => [

            'sandbox' => [
                'api_url' => env('ZOTAPAY_SANDBOX_API_URL'),
                'status_url' => env('ZOTAPAY_SANDBOX_STATUS_URL'),
                'notification_url' => env('ZOTAPAY_SANDBOX_NOTIFICATION_URL'),
                'notifications_server_ip' => env('ZOTAPAY_SANDBOX_NOTIFICATION_IP'),
                'end_point' => [
                    'cny' => env('ZOTAPAY_SANDBOX_CNY_ENDPOINT'),
                    'default' => env('ZOTAPAY_SANDBOX_DEFAULT_ENDPOINT'),
                ],
                'merchant_control' => env('ZOTAPAY_SANDBOX_CONTROL_KEY'),
                'redirect_url' => env('ZOTAPAY_SANDBOX_REDIRECT_URL'),
                'login' => env('ZOTAPAY_SANDBOX_LOGIN')
            ],

            'production' => [
                'api_url' => env('ZOTAPAY_PRODUCTION_API_URL'),
                'status_url' => env('ZOTAPAY_PRODUCTION_STATUS_URL'),
                'notification_url' => env('ZOTAPAY_PRODUCTION_NOTIFICATION_URL'),
                'notifications_server_ip' => env('ZOTAPAY_PRODUCTION_NOTIFICATION_IP'),
                'end_point' => [
                    'cny' => env('ZOTAPAY_PRODUCTION_CNY_ENDPOINT'),
                    'default' => env('ZOTAPAY_PRODUCTION_DEFAULT_ENDPOINT'),
                ],
                'merchant_control' => env('ZOTAPAY_PRODUCTION_CONTROL_KEY'),
                'redirect_url' => env('ZOTAPAY_PRODUCTION_REDIRECT_URL'),
                'login' => env('ZOTAPAY_PRODUCTION_LOGIN')
            ]

        ],

        'payeasy' => [

            'sandbox' => [
                'processor_url' => env('PAYEASY_SANDBOX_PROCESSOR_URL', 'https://api-test.payeasy.solutions/engine/hpp'),
                'api_url' => env('PAYEASY_SANDBOX_API_URL', 'https://api-test.payeasy.solutions/engine/rest/merchants'),
                'merchant_account_id' => env('PAYEASY_SANDBOX_MERCHANT_ACCOUNT_ID', '989184db-5e8a-45aa-85d8-145482bc7350'),
                'secret_key' => env('PAYEASY_SANDBOX_SANDBOX_SECRET_KEY', '31d99ed2-2106-426e-8a9a-dcccd734b9b4'),
                'success_redirect_url' => env('PAYEASY_SANDBOX_SUCCESS_REDIRECT_URL'),
                'fail_redirect_url' => env('PAYEASY_SANDBOX_FAIL_REDIRECT_URL'),
                'cancel_redirect_url' => env('PAYEASY_SANDBOX_CANCEL_REDIRECT_URL'),
                'processing_redirect_url' => env('PAYEASY_SANDBOX_PROCESSING_REDIRECT_URL'),
                'basic_authorization' => env('PAY_SANDBOX_BASIC_AUTHORIZATION', 'engine.realextate:xY6GyG2F9aUOBxLx')
            ],

            'production' => [
                'processor_url' => env('PAYEASY_PRODUCTION_PROCESSOR_URL'),
                'api_url' => env('PAYEASY_PRODUCTION_API_URL'),
                'merchant_account_id' => env('PAYEASY_PRODUCTION_MERCHANT_ACCOUNT_ID'),
                'secret_key' => env('PAYEASY_PRODUCTION_SECRET_KEY'),
                'success_redirect_url' => env('PAYEASY_PRODUCTION_SUCCESS_REDIRECT_URL'),
                'fail_redirect_url' => env('PAYEASY_PRODUCTION_FAIL_REDIRECT_URL'),
                'cancel_redirect_url' => env('PAYEASY_PRODUCTION_CANCEL_REDIRECT_URL'),
                'processing_redirect_url' => env('PAYEASY_PRODUCTION_PROCESSING_REDIRECT_URL'),
                'basic_authorization' => env('PAY_SANDBOX_BASIC_AUTHORIZATION', 'engine.realextate:xY6GyG2F9aUOBxLx')
            ]

        ]
    ]
];